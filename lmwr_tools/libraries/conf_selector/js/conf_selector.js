(function ($) {
    "use strict";
    Drupal.behaviors.lmwr_tools_conf_selector = {
        attach: function (context) {
            $( '.checkbox_groups legend' , context ).each( function(){

                if($(this).find('[type=checkbox]').length == 0 ){
                    var $checkbox = $('<input type="checkbox"/>');

                    $(this).prepend($checkbox);

                    $(this).click(Drupal.behaviors.lmwr_tools_conf_selector.toggleGroup);

                    // on click :
                    $($(this).parents('fieldset')[0]).find('.form-checkbox').change(function(){
                        Drupal.behaviors.lmwr_tools_conf_selector.initGroup($(this));
                    });
                }
                // init
                Drupal.behaviors.lmwr_tools_conf_selector.initGroup($(this));

            });
        },

        initGroup: function($element){
            var $fieldset = $($element.parents('fieldset')[0]);
            console.log($fieldset);

            if( $fieldset.find('.form-checkbox').not(':checked').length == 0){
                $fieldset.find('legend [type=checkbox]').prop('checked', true);
                console.log( $fieldset.find('legend [type=checkbox]') );
            }
            else{
                console.log( 'unchecked' );
                $fieldset.find('legend [type=checkbox]').prop('checked', false);
            }
        },

        toggleGroup:function(){
            console.log('ertjjj');
            $(this).parent().find('[type="checkbox"]').prop('checked' , $(this).find('[type="checkbox"]').is(':checked') );
        }
    };
})(jQuery);

