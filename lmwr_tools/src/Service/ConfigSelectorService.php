<?php

namespace Drupal\lmwr_tools\Service;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\lmwr_tools\Shared\CommandTrait;

/**
 * Class ConfigSelectorService.
 *
 * @package Drupal\lmwr_tools\Service
 */
class ConfigSelectorService {

  use CommandTrait;

  const SERVICE_NAME = 'lmwr_tools.config_selector_service';

  const DESTINATION_REP_NAME = '/../partials/';

  /**
   * The config manager service.
   *
   * @var ConfigManager
   */
  protected $configManager;

  /**
   * All conf.
   *
   * @var array
   */
  protected $allConf;

  /**
   * ConfigSelectorService constructor.
   *
   * @param \Drupal\Core\Config\ConfigManager $configManager
   *   The config manager.
   */
  public function __construct(ConfigManager $configManager) {
    $this->configManager = $configManager;
  }

  /**
   * Return the destination path.
   *
   * @return string
   *   The destination path.
   */
  public function getDestinationRepository() {
    return config_get_config_directory(CONFIG_SYNC_DIRECTORY) . static::DESTINATION_REP_NAME;
  }

  /**
   * Return the list of existing partials repo.
   *
   * @return array
   *   The existing partial repos.
   */
  public function getExistingPartialRepos() {
    $repoPath = $this->getDestinationRepository();

    if (is_dir($repoPath)) {
      // Get only directories list.
      $paths = array_filter(glob($repoPath . '*'), function ($path) {
        return is_dir($path);
      });

      // Get only names.
      $names = array_map(function ($path) {
        return basename($path);
      }, $paths);

      return array_combine($names, $names);
    }
    return [];
  }

  /**
   * Return the item of all config.
   *
   * @return array
   *   The conf items.
   */
  public function getAllCurrentConfig() {
    if (is_null($this->allConf)) {
      $this->allConf = $this->configManager->getConfigFactory()->listAll();

      // Sort.
      natsort($this->allConf);
    }
    return $this->allConf;
  }

  /**
   * Get the conf file in the repo.
   *
   * @param string $repoName
   *   The repos name.
   *
   * @return array
   *   The config list in the repo.
   */
  public function getExistingConfigInRepo($repoName) {
    if (is_dir($this->getDestinationRepository() . $repoName)) {

      $files = array_values(file_scan_directory($this->getDestinationRepository() . $repoName, '/.yml/', ['recurse' => FALSE], 0));
      $files = array_map(function ($data) {
        return $data->name;
      }, $files);

      // Sort.
      natsort($files);

      return empty($files) ? $this->getAllCurrentConfig() : $files;
    }
  }

  /**
   * Export the selection in the repo.
   *
   * @param array $confToExport
   *   THe config list to export.
   * @param string $repoName
   *   The repo name.
   */
  public function exportSelection(array $confToExport, $repoName) {

    // Create tmp repo for catching errors.
    $tmpName = $repoName . '_' . time();
    $this->prepareDestinationRepo($tmpName);

    // We get the conf data from sql query because we cannot get languaged data with the config manager.
    $languagedConf = $this->getLanguagedConfToExport($confToExport);
    try {

      // Get raw configuration data without overrides.
      foreach ($languagedConf as $confName => $collections) {
        // Collection is a language context.
        foreach ($collections as $collection => $configData) {
          $configName = sprintf('%s.yml', $confName);
          unset($configData['uuid']);
          $ymlData = Yaml::encode($configData);

          if ($collection == 'default') {
            $configFileName = sprintf('%s/%s', $this->getDestinationRepository() . $tmpName, $configName);
          }
          else {
            $destinationRepo = $tmpName . '/' . str_replace('.', '/', $collection);
            $this->prepareDestinationRepo($destinationRepo, FALSE);
            $configFileName = sprintf('%s/%s', $this->getDestinationRepository() . $destinationRepo, $configName);
          }
          file_put_contents($configFileName, $ymlData);
        }

      }

      // Rename tmp dir.
      $this->onFinishExport($repoName, $tmpName);

      // Delete system export.
      if (in_array('core.extension', $confToExport)) {
        $this->removeDevModulesFromExtensionList($this->getDestinationRepository() . $repoName);
      }

      \drupal_set_message(t('La configuration a été exportée correctement'));

    }
    catch (\Exception $e) {
      // Delete tmp dir.
      \drupal_set_message($e->getMessage(), 'error');
      file_unmanaged_delete_recursive($tmpName);
    }
  }

  /**
   * Get conf by language.
   *
   * @param array $confToExport
   *   THe conf name.
   *
   * @return array
   *   The conf.
   */
  protected function getLanguagedConfToExport(array $confToExport) {

    $query = \Drupal::database()->select('config', 'c');
    $query->fields('c', []);
    $query->condition('name', $confToExport, 'IN');

    $result = $query->execute()->fetchAll();
    $languagedConf = [];
    foreach ($result as $collection) {
      $collectionName = empty($collection->collection) ? 'default' : $collection->collection;
      $confName = $collection->name;
      $languagedConf[$confName][$collectionName] = unserialize($collection->data);
    }

    return $languagedConf;
  }

  /**
   * Create the tmp destination repo.
   *
   * @param string $repoName
   *   The repo name.
   */
  protected function prepareDestinationRepo($repoName, $deletePrevious = TRUE) {
    if ($deletePrevious && is_dir($this->getDestinationRepository() . $repoName)) {
      file_unmanaged_delete_recursive($this->getDestinationRepository() . $repoName);
    }

    // Create repo.
    $dir = $this->getDestinationRepository() . $repoName;
    file_prepare_directory($dir, FILE_CREATE_DIRECTORY);
  }

  /**
   * Delete the previous real repo and rename the tmp repo.
   *
   * @param string $realName
   *   The real destination repo name.
   * @param string $tmpName
   *   The tmp repo name.
   */
  protected function onFinishExport($realName, $tmpName) {
    if (is_dir($this->getDestinationRepository() . $realName)) {
      file_unmanaged_delete_recursive($this->getDestinationRepository() . $realName);
    }

    rename($this->getDestinationRepository() . $tmpName, $this->getDestinationRepository() . $realName);
  }

}
