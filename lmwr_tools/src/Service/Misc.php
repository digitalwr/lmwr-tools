<?php

namespace Drupal\lmwr_tools\Service;

use Cocur\Slugify\Slugify;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use \Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\lmwr_tools\Service\LanguageService;

/**
 * Class Misc.
 *
 * @package Drupal\lmwr_tools\Service
 */
class Misc {

  const SERVICE_NAME = 'lmwr_tools.misc';

  /**
   * Current page node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $currentPageNode;

  /**
   * Retourne le singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * Load Vocabulary Tree.
   *
   * @param string $vid
   *   Vocabulary ID to retrieve terms for.
   * @param int $parent
   *   The term ID under which to generate the tree.
   * @param int $max_depth
   *   The number of levels of the tree to return.
   *
   * @return array
   *   Return array
   */
  public function loadVocabularyTree($vid, $parent = 0, $max_depth = NULL) {

    // Get tree.
    /** @var \Drupal\taxonomy\TermStorage $taxonomy_storage */
    $taxonomy_storage = \Drupal::service('entity_type.manager')
      ->getStorage('taxonomy_term');
    $taxonomy = $taxonomy_storage->loadTree($vid, $parent, $max_depth, FALSE);

    // Get terms of the passed vid.
    $terms = $taxonomy_storage->loadByProperties(['vid' => $vid]);

    // Init result array.
    $result = [];

    foreach ($taxonomy as $taxo) {
      if ($taxo->depth == 0) {
        $result[$taxo->tid] = $terms[$taxo->tid];
      }
      else {
        $parent = reset($taxo->parents);

        if ($parent != 0 && array_key_exists($parent, $terms)) {
          if (!is_array($terms[$parent]->children)) {
            $terms[$parent]->children = [];
          }
          $terms[$parent]->children[$taxo->tid] = $terms[$taxo->tid];
        }
      }
    }
    return $result;
  }

  /**
   * Associe les enfants au terms parent.
   *
   * Associe les enfants dans $parentTerm->children dans la liste de tous les
   * enfants du term passé.
   *
   * @param \Drupal\taxonomy\Entity\Term $parentTerm
   *   Le terme parent.
   */
  public function initTreeForParentTerm(Term $parentTerm, $maxDepth = -1) {
    if ($parentTerm) {

      if (!$parentTerm->children) {
        /** @var \Drupal\taxonomy\TermStorage $termStorage */
        $termStorage = \Drupal::entityTypeManager()
          ->getStorage('taxonomy_term');
        $parentTerm->children = $termStorage->loadChildren($parentTerm->id());
      }

      if ($maxDepth !== 0) {
        /** @var Term $child */
        foreach ($parentTerm->children as $child) {
          $this->initTreeForParentTerm($child, $maxDepth - 1);
        }
      }
    }
  }

  /**
   * Return the parents of a term.
   *
   * @param Term $term
   *   The child term.
   *
   * @return Term[]
   *   The array of parent Terms (Can be multiple...)
   */
  public function getParentTerms(Term $term) {
    $storage = \Drupal::service('entity_type.manager')
      ->getStorage('taxonomy_term');
    return $storage->loadParents($term->id());
  }

  /**
   * Traduit l'arborescence de term dans la langue voulue.
   *
   * @param array $terms
   *   List de terme parents à parcourir.
   * @param string|null $languageId
   *   Le code de la langue de traduction. Traduit dans la langue courante si
   *   null.
   * @param string $mode
   *   Le mode de traduction par défaut de l'enfant.
   */
  public function translateTermTree(array &$terms, $languageId = NULL, $mode = LanguageService::MODE_DEFAULT_LANGUAGE_IF_NO_TRANSLATION_EXISTS) {
    /** @var Term $term */
    foreach ($terms as &$term) {
      $term = LanguageService::translate($term, $languageId, $mode);
      if (is_array($term->children)) {
        $this->translateTermTree($term->children, $languageId, $mode);
      }
    }
  }

  /**
   * Removes all link trails that are not siblings to the active trail.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeElement[] $tree
   *   The menu link tree to manipulate.
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement[]
   *   The manipulated menu link tree.
   */
  public function pruneActiveTree(array $tree) {
    $activeTree = [];

    foreach ($tree as $treeElement) {
      if ($treeElement->inActiveTrail) {
        $activeTree = $treeElement->subtree;
        break;
      }
    }
    return $activeTree;
  }

  /**
   * Removes all link trails that are not siblings to the active trail.
   *
   * For a menu such as:
   * Parent 1
   *  - Child 1
   *  -- Child 2
   *  -- Child 3
   *  -- Child 4
   *  - Child 5
   * Parent 2
   *  - Child 6
   * with current page being Child 3, Parent 2, Child 6, and Child 5 would be
   * removed.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeElement[] $tree
   *   The menu link tree to manipulate.
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement[]
   *   The manipulated menu link tree.
   */
  public function removeInactiveTrail(array $tree) {
    // Get the current item's parent ID.
    $current_item_parent = $this->getCurrentParent($tree);

    // Tree becomes the current item parent's children if the current item
    // parent is not empty. Otherwise, it's already the "parent's" children
    // since they are all top level links.
    if (!empty($current_item_parent)) {
      $tree = $current_item_parent->subtree;
    }
    else {
      return [];
    }

    // Strip children from everything but the current item, and strip children
    // from the current item's children.
    $tree = $this->stripChildren($tree);

    // Return the tree.
    return $tree;
  }

  /**
   * Get the parent of the current active menu link.
   *
   * Get the parent of the current active menu link, or return NULL if the
   * current active menu link is a top-level link.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeElement[] $tree
   *   The tree to pull the parent link out of.
   * @param \Drupal\Core\Menu\MenuLinkTreeElement|null $prev_parent
   *   The previous parent's parent, or NULL if no previous parent exists.
   * @param \Drupal\Core\Menu\MenuLinkTreeElement|null $parent
   *   The parent of the current active link, or NULL if not parent exists.
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement|null
   *   The parent of the current active menu link, or NULL if no parent exists.
   */
  private function getCurrentParent(array $tree, $prev_parent = NULL, $parent = NULL) {
    // Get active item.
    foreach ($tree as $leaf) {
      if ($leaf->inActiveTrail) {
        $active_item = $leaf;
        break;
      }
    }

    // If the active item is set and has children.
    if (!empty($active_item) && !empty($active_item->subtree)) {
      // Run getCurrentParent with the parent ID as the $active_item ID.
      return $this->getCurrentParent(
        $active_item->subtree, $parent, $active_item
      );
    }

    // If the active item is not set, we know there was no active item on this.
    // Level therefore the active item parent is the previous level's parent.
    if (empty($active_item)) {
      return $prev_parent;
    }

    // Otherwise, the current active item has no children to check, so it is
    // the bottommost and its parent is the correct parent.
    return $parent;
  }

  /**
   * Remove the children from all MenuLinkTreeElements that aren't active.
   *
   * If it is active, remove its children's children.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeElement[] $tree
   *   The menu links to strip children from non-active leafs.
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement[]
   *   A menu tree with no children of non-active leafs.
   */
  private function stripChildren(array $tree) {
    // For each item in the tree, if the item isn't active, strip its children
    // and return the tree.
    foreach ($tree as &$leaf) {
      // Check if active and if has children.
      if ($leaf->inActiveTrail && !empty($leaf->subtree)) {
        // Then recurse on the children.
        $leaf->subtree = $this->stripChildren($leaf->subtree);
      }
      // Otherwise, if not the active menu.
      elseif (!$leaf->inActiveTrail) {
        // Otherwise, it's not active, so we don't want to display any children
        // so strip them.
        $leaf->subtree = array();
      }
    }

    return $tree;
  }

  /**
   * Turn a string into CamelCase.
   *
   * @param string $data
   *   The string to convert.
   *
   * @return string
   *   The converted string.
   *
   * @throws \Exception
   */
  static public function toCamelCase($data) {
    if (is_string($data)) {
      $slug = (new Slugify())->slugify($data);
      $replacement = [
        '-' => ' ',
        '_' => ' ',
      ];
      return str_replace(' ', '', ucwords(str_replace(array_keys($replacement), $replacement, $slug)));
    }
    throw new \Exception('Element is not convertible to CamelCase');
  }

  /**
   * Return the node of the current page.
   *
   * @return \Drupal\Node\Entity\Node|null
   *   The current node.
   */
  static public function getCurrentPageNode() {
    if (!isset(static::me()->currentPageNode)) {
      if ($node = \Drupal::routeMatch()->getParameter('node')) {
        if (is_numeric($node)) {
          static::me()->currentPageNode = LanguageService::load('node', $node);
        }
        elseif ($node instanceof Node) {
          static::me()->currentPageNode = LanguageService::translate($node);
        }
      }
    }

    return static::me()->currentPageNode;
  }

  /**
   * Retourne la liste des ids des éléments référencés.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemList $field
   *   The field.
   * @param string $attribute
   *   The attributes.
   *
   * @return array
   *   The list of ids.
   */
  public function getIdsFromReferenceFieldList(EntityReferenceFieldItemList $field, $attribute = 'target_id') {
    $result = [];
    foreach ($field->getIterator() as $item) {
      $result[] = $item->{$attribute};
    }

    return $result;
  }

}
