<?php

namespace Drupal\lmwr_tools\Service;

use Drupal\Core\Form\FormStateInterface;
use Drupal\lmwr_tools\Plugin\Field\FieldType\LmwrThemeTemplateFieldType;
use Drupal\lmwr_tools\Plugin\Field\FieldWidget\LmwrThemeTemplateFieldWidget;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Class Misc.
 *
 * @package Drupal\lmwr_tools\Service
 */
class Templates {

  const SERVICE_NAME = 'lmwr_tools.templates';

  /**
   * Return singleton.
   *
   * @return static
   *   Static object.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * Return current theme page templates.
   *
   * @return array
   *   Themes.
   */
  public function getPageTemplates() {
    $config = \Drupal::config('system.theme');
    $currentTheme = $config->get('default');

    $themesInfos = \Drupal::service('theme_handler')->listInfo();
    $currentThemeData = $themesInfos[$currentTheme];

    $currentThemeInfo = $currentThemeData->info;

    if (!empty($currentThemeInfo['pages-templates'])) {
      return $currentThemeInfo['pages-templates'];
    }

    return [];
  }

  /**
   * Hook callback.
   *
   * @param array $form
   *   Form.
   * @param FormStateInterface $form_state
   *   Form state.
   */
  public function hookFormNodeFormAlter(array &$form, FormStateInterface $form_state) {
    foreach ($form as &$item) {
      if (is_array($item) && isset($item['widget']) && isset($item['widget'][0]['value']['#id']) && $item['widget'][0]['value']['#id'] === LmwrThemeTemplateFieldWidget::ID) {
        $form['page_template_settings'] = [
          '#type' => 'details',
          '#title' => t('Page template'),
          '#group' => 'advanced',
        ];
        $item['#group'] = 'page_template_settings';
      }
    }
  }

}
