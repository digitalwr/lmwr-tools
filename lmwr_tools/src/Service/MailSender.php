<?php

namespace Drupal\lmwr_tools\Service;

use Drupal\Core\Render\Markup;

/**
 * Class MailSender.
 *
 * Massage des données pour l'envoie depuis swiftmailer.
 *
 * @package Drupal\lmwr_tools\Service
 */
class MailSender {

  /**
   * Service name.
   *
   * @var string
   */
  const SERVICE_NAME = 'lmwr_tools.mail_sender';

  /**
   * Champs pour overrider les headers.
   *
   * @var string
   */
  const FIELD_HEADERS_OVERRIDE = 'headers_override';

  /**
   * From.
   *
   * @var string
   */
  const FIELD_FROM = 'from';

  /**
   * Le service d'environnement.
   *
   * @var EnvironmentService
   */
  protected $environment;

  /**
   * Destinataire staging.
   *
   * @var string
   */
  protected $stagingRecipient;

  /**
   * Liste des champs à overrider par le destinataire staging.
   *
   * @var array
   */
  protected static $receiverHeadersField = ['To', 'Sender', 'Cc', 'Bcc'];

  /**
   * MailSender constructor.
   */
  public function __construct() {
    $this->initStagingRecepient();
  }

  /**
   * Retourne le singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * Initialise le recipient de staging.
   */
  protected function initStagingRecepient() {
    $this->environment = \Drupal::service(EnvironmentService::SERVICE_NAME);
    $this->stagingRecipient = \Drupal::config('lmwr.settings')
      ->get('staging_mail_recipient');
  }

  /**
   * Prépare le mail avant envoi.
   *
   * @param string $key
   *   L'id de l'email.
   * @param array $message
   *   Les données du message.
   * @param array $params
   *   Le paramètre du message.
   */
  public function prepareMail($key, array &$message, array $params) {

    // Gestion de la config d'envoi.
    $this->initMailConfig($message);

    // Initialisation du sujet de mail.
    $this->initMessageData($message, $params);

    // Initialisation des destinataires.
    $this->initRecipients($message);

    // Initialise le sender.
    $this->initSender($message, $params);

    // Gestion des headers spécifiques.
    $this->initOverridenHeaders($message, $params);

    // Gestion des attachements.
    $this->initAttachments($message, $params);

    // Gestion de l'environnement.
    if ($this->environment->isStaging()) {
      $this->initStagingProcess($message);
    }

    $context = ['key' => $key];
    \Drupal::moduleHandler()->alter('lmwr_tools_mail', $message, $context);
  }

  /**
   * Initialisation de la config.
   *
   * @param array $message
   *   Les données du message.
   */
  protected function initMailConfig(array &$message) {
    if ($config = \Drupal::config('swiftmailer.message')) {
      $format = $config->get('format') ?: SWIFTMAILER_FORMAT_HTML;
      $charset = $config->get('character_set') ?: SWIFTMAILER_VARIABLE_CHARACTER_SET_DEFAULT;
      $message['headers']['Content-Type'] = $format . '; charset="' . $charset . '"';
    }
    else {
      $message['headers']['Content-Type'] = SWIFTMAILER_FORMAT_HTML . '; charset="' . SWIFTMAILER_VARIABLE_CHARACTER_SET_DEFAULT . '"';
    }
  }

  /**
   * Retourne le recipient de staging si on est en staging.
   *
   * @param mixed $recipients
   *   Le récipient théorique.
   *
   * @return mixed
   *   Le vrai récipient.
   */
  protected function getRealRecepients($recipients) {
    if ($this->environment->isStaging()) {
      return $this->stagingRecipient;
    }
    return $recipients;
  }

  /**
   * Initialise l'objet du mail.
   *
   * @param array $message
   *   Les données du message.
   * @param array $params
   *   Les paramètres du message.
   */
  protected function initMessageData(array &$message, array $params) {
    $message['subject'] = $params['subject'];
    $message['body'] = array_map(function ($text) {
      return Markup::create($text);
    }, [$params['message']]);
  }

  /**
   * Initialise le destinataire.
   *
   * @param array $message
   *   Les données du message.
   */
  protected function initRecipients(array &$message) {
    if (is_array($message["to"])) {
      $message['to'] = implode(',', $message["to"]);
    }
  }

  /**
   * Initialise le sender.
   *
   * @param array $message
   *   Les données du message.
   * @param array $params
   *   Les paramètres du message.
   */
  protected function initSender(array &$message, array $params) {
    if (array_key_exists(static::FIELD_FROM, $params) && !empty($params[static::FIELD_FROM])) {
      $from = $params[static::FIELD_FROM];
      $message['from'] = $from;
      $message['headers']['From'] = $from;
      $message['headers']['Sender'] = $from;
      $message['headers']['Reply-to'] = $from;
      $message['headers']['Return-path'] = $from;
    }
  }

  /**
   * Initialise l'override de headers.
   *
   * @param array $message
   *   Les données du message.
   * @param array $params
   *   Les paramètres du message.
   */
  protected function initOverridenHeaders(array &$message, array $params) {
    if (array_key_exists(static::FIELD_HEADERS_OVERRIDE, $params) && is_array($params[static::FIELD_HEADERS_OVERRIDE])) {
      $headers_override = array_filter($params[static::FIELD_HEADERS_OVERRIDE]);
      foreach ($headers_override as $key => $value) {
        $message['headers'][$key] = $value;
      }
    }
  }

  /**
   * Initialise les attachments.
   *
   * @param array $message
   *   Les données du message.
   * @param array $params
   *   Les paramètres du message.
   */
  protected function initAttachments(array &$message, array $params) {
    if (array_key_exists('files', $params) && !empty($params['files'])) {
      $message['files'][] = $params['files'];
    }
  }

  /**
   * Initialise les données fonction de la conf des recepteurs en mode staging.
   *
   * @param array $message
   *   Les données du message.
   */
  protected function initStagingProcess(array &$message) {
    // Récupération des recepteurs théoriques.
    $theoricRecipient = $message['to'];
    $message['to'] = $this->stagingRecipient;

    // Initialisation des recepteurs pratiques.
    foreach (static::$receiverHeadersField as $key => $value) {
      if (array_key_exists($key, $message['headers'])) {
        $message['headers'][$key] = $this->stagingRecipient;
      }
    }

    // Ajout de test dans le sujet.
    $message['subject'] = '[TEST] ' . $message['subject'];

    // Ajout des récipient dans le body.
    $message['body'][] = Markup::create('<hr>[RECIPIENTS]: ' . $theoricRecipient);
  }

}
