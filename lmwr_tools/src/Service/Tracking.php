<?php

namespace Drupal\lmwr_tools\Service;

use Drupal\Core\Database\Database;

/**
 * Tracking class.
 */
class Tracking {

  /**
   * Service name.
   *
   * @var string
   */
  const SERVICE_NAME = 'lmwr_tools.lmwr_tracking';

  /**
   * Endpoint.
   *
   * @var string
   */
  const ENDPOINT = 'https://extranet.lmwr.fr/api/websites/tracker?_format=json';

  /**
   * Singleton.
   *
   * @return static
   *   Static class.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * Return webslites list.
   *
   * @return array|null
   *   Array of websites.
   */
  public function getList() {
    $client = \Drupal::httpClient();

    $request = $client->get(static::ENDPOINT, ['headers' => ['Content-Type' => 'application/json']]);
    if ($request->getStatusCode() == 200) {
      $response = (string) $request->getBody();
      return json_decode($response);
    }

    return NULL;
  }

  /**
   * Return current project ID.
   *
   * @return string|null
   *   Config object.
   */
  public function getCurrentProjectId() {
    return \Drupal::state()->get('lmwr.tracker');
  }

  /**
   * Set current project ID.
   *
   * @param string $projectId
   *   Project ID.
   */
  public function setProjectId($projectId) {
    \Drupal::state()->set('lmwr.tracker', $projectId);
  }

  /**
   * Send data at extranet endpoint.
   */
  public function sendData() {
    $data = $this->getStatusData();

    $client = \Drupal::httpClient();

    $request = $client->post(static::ENDPOINT, ['json' => $data], ['headers' => ['Content-Type' => 'application/json']]);

    if (!$request->getStatusCode() == 200) {
      \Drupal::logger('lmwr_tools')->error('Tracking failed!');
    }
  }

  /**
   * Return status data.
   *
   * @return array
   *   Data array.
   */
  protected function getStatusData() {
    $connectionOptions = Database::getConnection()->getConnectionOptions();
    $sql = "SELECT  SUM(ROUND(((DATA_LENGTH + INDEX_LENGTH) / 1024 / 1024 ), 2)) FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = :tableName";
    $dbSize = \Drupal::database()->query($sql, array(':tableName' => $connectionOptions['database']))->fetchField();

    $outputData = array(
      'id' => $this->getCurrentProjectId(),
      'version' => \Drupal::VERSION,
      'db_updates' => 0,
      'db_size' => $dbSize,
      'security_updates' => array(),
      'url' => $GLOBALS['base_url'],
    );

    if ($available = update_get_available(TRUE)) {
      $data = update_calculate_project_data($available);
      foreach ($data as $project) {
        $status = $project['status'];
        if ($status != UPDATE_CURRENT && $status == UPDATE_NOT_SECURE) {
          $outputData['security_updates'][] = array(
            'title' => $project['title'],
            'name' => $project['name'],
            'type' => $project['project_type'],
            'latest_version' => $project['latest_version'],
            'recommended' => $project['recommended'],
            'existing_version' => $project['existing_version'],
          );
        }
      }
    }

    $outputData['latest_version'] = $data['drupal']['latest_version'];
    $outputData['recommended_version'] = $data['drupal']['recommended'];

    return $outputData;
  }

}
