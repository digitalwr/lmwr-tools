<?php

namespace Drupal\lmwr_tools\Service;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;

/**
 * Class CacheAuto.
 *
 * @package Drupal\lmwr_tools\Service
 */
class CacheAuto {

  const SERVICE_NAME = 'lmwr_tools.cache_auto';
  const KEY_PARENT_TYPE = 'parentContentType';
  const KEY_FIELD_NAME = 'fieldName';

  /**
   * Liste des données de référencement par type.
   *
   * @var array
   *   Liste des données de référencement par type.
   */
  protected $tables = [];

  /**
   * Liste des entités en cours d'invalidation.
   *
   * @var array
   *   Liste des entités en cours d'invalidation.
   */
  protected $processingEntities = [];

  /**
   * AllReferencerFields.
   *
   * @var array
   */
  protected $allReferencerFields;

  /**
   * Invalide le cache et appelle récursivement l'invalidation des caches des entités référencentes.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   L'entité référencée.
   */
  public function invalidateCache(ContentEntityInterface $entity) {
    // On vérifie que l'entité n'est pas déjà encourt d'export pour ne pas boucler.
    if (!in_array($entity->uuid(), $this->processingEntities)) {
      $this->processingEntities[] = $entity->uuid();

      foreach ($this->getReferencers($entity) as $referencer) {
        // On invalide le cache de chaque référenceur.
        $this->invalidateCache($referencer);
      }

      // On invalide le cache de l'entité même.
      Cache::invalidateTags([
        $entity->getEntityType()
          ->id() . ':' . $entity->id()
      ]);
    }
  }

  /**
   * Retourne la liste des référenceur de l'entité.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   L'entité.
   *
   * @return array
   *   Les référenceurs.
   */
  public function getReferencers(ContentEntityInterface $entity) {

    // Récupération des champs de type entity_reference.
    $entityReferenceFields = $this->getReferenceTables($entity->getEntityType());

    // Pour chaque type de contenu référençant on va chercher ceux
    // qui référencent l'entité courante.
    $referencers = [];
    foreach ($entityReferenceFields as $referencerData) {
      // On récupère la liste des référenceurs.
      $referencers = array_merge($referencers, $this->getReferencersByType($entity, $referencerData));
    }

    return $referencers;
  }

  /**
   * Retourne le tableau de données des entités référencantes en fonctino du type de l'entité référencée.
   *
   * @param \Drupal\Core\Entity\ContentEntityTypeInterface $entityType
   *   Le type d'entité.
   *
   * @return mixed
   *   Le tableau de données pour le type passé.
   */
  protected function getReferenceTables(ContentEntityTypeInterface $entityType) {
    if (!in_array($entityType->id(), $this->tables)) {

      // ON récupère les champs qui font des références.
      $allReferencedFields = $this->getAllReferencerFields();

      // Pour chaque type de contenu ayant un entity_reference.
      foreach ($allReferencedFields as $contentTypeName => $values) {
        // Pour chaque champs de type entity_reference.
        foreach ($values as $fieldName => $fieldData) {
          // On récupère les target_type en fonction des bundles.
          foreach ($fieldData['bundles'] as $bundle) {
            // Récupération des données du field.
            $fieldDefinition = $this->getDefinitionsByTypeAndBundle($contentTypeName, $bundle)[$fieldName];
            $settings = $fieldDefinition->getSettings();

            // récupération de la tableName.
            if ($settings['target_type'] == $entityType->id()) {
              if (!array_key_exists($entityType->id(), $this->tables)) {
                $this->tables[$entityType->id()] = [];
              }

              if (!array_key_exists($fieldName, $this->tables[$entityType->id()])) {
                // On ajoute les données du champs et du type de contenu référencant à l'index du type
                // d'entité référencée.
                $this->tables[$entityType->id()][$fieldName] = [
                  static::KEY_PARENT_TYPE => $contentTypeName,
                  static::KEY_FIELD_NAME  => $fieldName
                ];
              }
            }
          }
        }
      }
    }

    if (array_key_exists($entityType->id(), $this->tables)) {
      return $this->tables[$entityType->id()];
    }
    return [];
  }

  /**
   * Retourne la definitions des fields par type and bundle.
   *
   * @param string $contentTypeName
   *   Le type de contenu.
   * @param string $bundle
   *   Le bundle.
   *
   * @return mixed
   *   Les données du bundle.
   */
  public function getDefinitionsByTypeAndBundle($contentTypeName, $bundle) {
    return \Drupal::service('entity_field.manager')
      ->getFieldDefinitions($contentTypeName, $bundle);
  }

  /**
   * Retourne les référenceurs par type.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   L'entité référencée.
   * @param array $referencerData
   *   Les données de référencement par type.
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]
   *   Les référenceurs.
   */
  protected function getReferencersByType(ContentEntityInterface $entity, array $referencerData) {
    try {
      $referencerType = $referencerData[static::KEY_PARENT_TYPE];
      $fieldName = $referencerData[static::KEY_FIELD_NAME];
      $query = \Drupal::entityQuery($referencerType);
      $query->condition($fieldName . '.target_id', $entity->id());
      $referencersIds = $query->execute();
      $referencers = \Drupal::entityTypeManager()
        ->getStorage($referencerType)
        ->loadMultiple($referencersIds);
    }
    catch (\Exception $e) {
      $referencers = [];
    }
    return $referencers;
  }

  /**
   * Retourne tous les champs de type référence.
   *
   * @return array
   *   La liste des champs référence.
   */
  protected function getAllReferencerFields() {
    if (is_null($this->allReferencerFields)) {
      $referencerTypes = ['entity_reference', 'entity_reference_revisions'];

      $this->allReferencerFields = [];

      // On ajoute chaque type de champ au tableau des entityType.
      /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager */
      $entityFieldManager = \Drupal::service('entity_field.manager');
      foreach ($referencerTypes as $referencerType) {
        $typeInstance = $entityFieldManager->getFieldMapByFieldType($referencerType);

        foreach ($typeInstance as $entityTypeId => $fieldData) {
          if (!isset($this->allReferencerFields[$entityTypeId])) {
            $this->allReferencerFields[$entityTypeId] = $fieldData;
          }
          elseif (!empty($fieldData)) {
            $this->allReferencerFields[$entityTypeId] = array_merge($this->allReferencerFields[$entityTypeId], $fieldData);
          }
        }
      }
    }

    return $this->allReferencerFields;
  }

}
