<?php

namespace Drupal\lmwr_tools\Service;

/**
 * Class YoutubeTwigFilters.
 *
 * @package lmwr_tools
 */
class YoutubeTwigFilters extends \Twig_Extension {

  /**
   * The list of Youtube url patterns.
   *
   * @var string
   *   Youube pattern.
   */
  const PATTERN =
    '%^# Match any YouTube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char YouTube id.
        $%x';

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter(
        'youtubeThumbnail', array($this, 'youtubeThumbnail')
      ),
      new \Twig_SimpleFilter(
        'youtubeEmbedUrl', array($this, 'generateYoutubeEmbedUrl')
      ),
    ];
  }

  /**
   * Try to get a thumbnail for the given YouTube video's URL.
   *
   * @param string $url
   *   The URL of the video.
   * @param string $class
   *   CSS class to give to the thumbnail (optional).
   *
   * @return array
   *   Render array of the image.
   *
   * @throws \Exception
   */
  public function youtubeThumbnail($url, $class = '') {
    if (!is_string($url) || !is_string($class)) {
      throw new \Exception('Entry parameters must be string');
    }

    $thumbnail = ['#markup' => ''];

    $uri = preg_replace(
      self::PATTERN, 'https://img.youtube.com/vi/$1/0.jpg', $url
    );

    if ($uri == $url) {
      return $thumbnail;
    }

    $thumbnail = [
      '#theme'      => 'image',
      '#uri'        => $uri,
      '#alt'        => 'Youtube video thumbnail',
      '#attributes' => ['class' => [$class]],
    ];

    return $thumbnail;
  }

  /**
   * Generates a youtube embed URL from the given one.
   *
   * @param string $url
   *   The Youtube URL to format as embed URL.
   * @param array $params
   *   A set of GET parameters to include in URL.
   * @param bool $useNoCookie
   *   Use no cookie version.
   *
   * @return string
   *   The formatted URL.
   *
   * @throws \Exception
   */
  public function generateYoutubeEmbedUrl($url, array $params = array(), $useNoCookie = FALSE) {
    if (!is_string($url)) {
      throw new \Exception(
        sprintf('Parameter $url must be a string, %s given', gettype($url))
      );
    }

    $replacement = 'https://www.youtube.com/embed/$1';
    if ($useNoCookie) {
      $replacement = 'https://www.youtube-nocookie.com/embed/$1';
    }

    $uri = preg_replace(
      self::PATTERN, $replacement, $url
    );

    // Add optional parameters to URL.
    if (!empty($params)) {
      $uri .= '?' . http_build_query($params);
    }

    return $uri;
  }

}
