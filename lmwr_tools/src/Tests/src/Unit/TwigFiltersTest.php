<?php

namespace Drupal\lmwr_tools\Tests;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\lmwr_tools\Service\TwigFilters;
use Drupal\Tests\UnitTestCase;

/**
 * Contains all tests of service TwigFilters.
 *
 * @group lmwr_tools
 */
class TwigFiltersTest extends UnitTestCase {

  /**
   * Local instance of service.
   *
   * @var TwigFilters
   */
  private $service;

  /**
   * Before each test.
   */
  public function setUp() {
    $entityManager = $this->getMock(EntityTypeManagerInterface::class);

    $this->service = new TwigFilters($entityManager);
  }

  /**
   * After each test.
   */
  public function tearDown() {
    $this->service = NULL;
  }

  /**
   * Tests method maxLength() with bad parameter type.
   */
  public function testMaxLengthBadParameter() {
    $content = array('foo');

    $actual = $this->service->maxLength($content);
    $this->assertEquals($content, $actual);
  }

  /**
   * Provides test data for testMaxLength().
   *
   * @return array
   *   A list of arrays composed as [data, expected_result].
   */
  public function provideStrings() {
    return [
      ['aaa', 'aaa'],
      [str_repeat('abc', 120), 'abcabcabcabcabcab...'],
      ['aaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaa'],
      ['aaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaa...'],
    ];
  }

  /**
   * Tests method maxLength() with several strings.
   *
   * @dataProvider provideStrings
   */
  public function testMaxLength($string, $expected) {
    $actual = $this->service->maxLength($string);

    $this->assertEquals($expected, $actual);
  }

  /**
   * Provides test data for testHalfRound().
   *
   * @return array
   *   Test data.
   */
  public function numberProvider() {
    return [
      ['azerty', 'azerty'],
      [123, 123],
      [23.4123, 23.5],
      [23.0, 23],
      [-23.4123, -23.4123],
    ];
  }

  /**
   * Test method halfRound() with bad parameter type.
   *
   * @expectedException Exception
   * @expectedExceptionMessage Invalid parameter type
   */
  public function testHalfRoundBadParam() {
    $data = array();

    $this->service->halfRound($data);
  }

  /**
   * Test method halfRound() in several cases.
   *
   * @dataProvider numberProvider
   */
  public function testHalfRound($data, $expected) {
    $actual = $this->service->halfRound($data);

    $this->assertEquals($expected, $actual);
  }

}
