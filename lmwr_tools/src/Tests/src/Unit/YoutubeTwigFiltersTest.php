<?php

namespace Drupal\lmwr_tools\Tests;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\lmwr_tools\Service\YoutubeTwigFilters;
use Drupal\Tests\UnitTestCase;

/**
 * Contains all tests of service YoutubeTwigFiltersTest.
 *
 * @group lmwr_tools
 */
class YoutubeTwigFiltersTest extends UnitTestCase {

  /**
   * Local instance of service.
   *
   * @var YoutubeTwigFilters
   */
  private $service;

  /**
   * Before each test.
   */
  public function setUp() {
    $entityManager = $this->getMock(EntityTypeManagerInterface::class);

    $this->service = new YoutubeTwigFilters($entityManager);
  }

  /**
   * After each test.
   */
  public function tearDown() {
    $this->service = NULL;
  }

  /**
   * Provides test data for method testGenerateYoutubeEmbedUrl().
   *
   * Each data set is composed of : [test param1, test param2, expected result].
   */
  public function urlProvider() {
    return [

      // Data set 0.
      [
        'foo',
        [],
        'foo',
      ],

      // Data set 1.
      [
        'foo',
        ['foo' => 'bar'],
        'foo?foo=bar'
      ],

      // Data set 2.
      [
        'https://youtu.be/HRnLlhJoZSw',
        [],
        'https://www.youtube.com/embed/HRnLlhJoZSw'
      ],

      // Data set 3.
      [
        'https://youtu.be/HRnLlhJoZSw#fooAnchor?foo=bar&bar=foo',
        [],
        'https://www.youtube.com/embed/HRnLlhJoZSw'
      ],

      // Data set 4.
      [
        'https://youtu.be/HRnLlhJoZSw',
        ['foo' => 'bar'],
        'https://www.youtube.com/embed/HRnLlhJoZSw?foo=bar'
      ],

      // Data set 5.
      [
        'https://www.youtube.com/watch?v=HRnLlhJoZSw',
        [],
        'https://www.youtube.com/embed/HRnLlhJoZSw'
      ],

      // Data set 6.
      [
        'https://www.youtube.com/watch?v=HRnLlhJoZSw',
        [
          'foo' => 'bar',
          'bar' => 'foo',
        ],
        'https://www.youtube.com/embed/HRnLlhJoZSw?foo=bar&bar=foo',
      ],

      // Data set 7.
      [
        'https://www.youtube.com/watch?v=HRnLlhJoZSw#fooAnchor',
        [],
        'https://www.youtube.com/embed/HRnLlhJoZSw',
      ],

      // Data set 8.
      [
        'https://www.youtube.com/embed/HRnLlhJoZSw',
        [],
        'https://www.youtube.com/embed/HRnLlhJoZSw',
      ],

      // Data set 9.
      [
        'https://www.youtube.com/embed/HRnLlhJoZSw#fooAnchor?foo=bar&bar=foo',
        [],
        'https://www.youtube.com/embed/HRnLlhJoZSw',
      ],

      // Data set 10.
      [
        'www.youtube.com/watch?v=B0_5ATOLjc4',
        [],
        'https://www.youtube.com/embed/B0_5ATOLjc4',
      ],

      // Data set 11.
      [
        'youtu.be/HRnLlhJoZSw',
        [],
        'https://www.youtube.com/embed/HRnLlhJoZSw'
      ],

      // Data set 12.
      [
        'youtu.be/HRnLlhJoZSw#fooAnchor?foo=bar&bar=foo',
        [],
        'https://www.youtube.com/embed/HRnLlhJoZSw'
      ],

      // Data set 4.
      [
        'youtu.be/HRnLlhJoZSw',
        ['foo' => 'bar'],
        'https://www.youtube.com/embed/HRnLlhJoZSw?foo=bar'
      ],

      // Data set 5.
      [
        'www.youtube.com/watch?v=HRnLlhJoZSw',
        [],
        'https://www.youtube.com/embed/HRnLlhJoZSw'
      ],

      // Data set 6.
      [
        'www.youtube.com/watch?v=HRnLlhJoZSw',
        [
          'foo' => 'bar',
          'bar' => 'foo',
        ],
        'https://www.youtube.com/embed/HRnLlhJoZSw?foo=bar&bar=foo',
      ],

      // Data set 7.
      [
        'www.youtube.com/watch?v=HRnLlhJoZSw#fooAnchor',
        [],
        'https://www.youtube.com/embed/HRnLlhJoZSw',
      ],

      // Data set 8.
      [
        'www.youtube.com/embed/HRnLlhJoZSw',
        [],
        'https://www.youtube.com/embed/HRnLlhJoZSw',
      ],

      // Data set 9.
      [
        'www.youtube.com/embed/HRnLlhJoZSw#fooAnchor?foo=bar&bar=foo',
        [],
        'https://www.youtube.com/embed/HRnLlhJoZSw',
      ],

      // Data set 10.
      [
        'www.youtube.com/watch?v=B0_5ATOLjc4',
        [],
        'https://www.youtube.com/embed/B0_5ATOLjc4',
      ],
    ];
  }

  /**
   * Test method youtubeThumbnail() with bad parameters.
   *
   * @expectedException Exception
   * @expectedExceptionMessage Entry parameters must be string
   */
  public function testYoutubeThumbnailException() {
    $url = [];
    $class = [];

    $this->service->youtubeThumbnail($url, $class);
  }

  /**
   * Test method youtubeThumbnail() in different cases.
   */
  public function testYoutubeThumbnail() {
    // Invalid URL without class attribute.
    $actual = $this->service->youtubeThumbnail('foo');
    $expected = ['#markup' => ''];

    $this->assertEquals($expected, $actual);

    // Invalid URL with class attribute.
    $actual = $this->service->youtubeThumbnail('foo', 'foo');
    $expected = ['#markup' => ''];

    $this->assertEquals($expected, $actual);

    // Valid URL with class attribute.
    $actual = $this->service->youtubeThumbnail('http://youtube.com/embed/foo', 'foo');
    $expected = [
      '#theme'      => 'image',
      '#uri'        => 'https://img.youtube.com/vi/foo/0.jpg',
      '#alt'        => 'Youtube video thumbnail',
      '#attributes' => [
        'class' => ['foo'],
      ]
    ];

    $this->assertEquals($expected, $actual);

    // Valid URL without class attribute.
    $actual = $this->service->youtubeThumbnail('http://youtube.com/embed/foo');
    $expected = [
      '#theme'      => 'image',
      '#uri'        => 'https://img.youtube.com/vi/foo/0.jpg',
      '#alt'        => 'Youtube video thumbnail',
      '#attributes' => [
        'class' => [''],
      ]
    ];

    $this->assertEquals($expected, $actual);

    // Another valid URL format.
    $actual = $this->service->youtubeThumbnail('https://www.youtube.com/watch?v=H7LA9yM8USk');
    $expected = [
      '#theme'      => 'image',
      '#uri'        => 'https://img.youtube.com/vi/H7LA9yM8USk/0.jpg',
      '#alt'        => 'Youtube video thumbnail',
      '#attributes' => [
        'class' => [''],
      ]
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * Test method generateYoutubeEmbedUrl() with bad entry parameter.
   *
   * @expectedException Exception
   * @expectedExceptionMessage Parameter $url must be a string, array given
   */
  public function testGenerateYoutubeEmbedUrlException() {
    $this->service->generateYoutubeEmbedUrl(array('foo'));
  }

  /**
   * Test method generateYoutubeEmbedUrl().
   *
   * @dataProvider urlProvider
   */
  public function testGenerateYoutubeEmbedUrl($url, $params, $expected) {
    $actual = $this->service->generateYoutubeEmbedUrl($url, $params);

    $this->assertEquals($expected, $actual);
  }

}
