<?php

namespace Drupal\lmwr_tools;

use Drupal\Core\Extension\Extension;

/**
 * Class LmwrGeneratePo.
 *
 * @package lmwr_tools
 */
class LmwrGeneratePo {

  protected static $twigPatterns = [
    'simple'       => [
      'pattern' => '/{{( *)(\'|"){1}([^\}\}\{\{]*)(\'|"){1}( *)\|( *)t( *)}}/',
      'index'   => 3,
    ],
    'setvar'       => [
      'pattern' => '/{%([a-zA-Z=:\s]*)( *)=( *)(\'|"){1}([^\}\}\{\{]*)(\'|"){1}( *)\|( *)t( *)%}/',
      'index'   => 5,
    ],
    'setvar_array' => [
      'pattern' => '/([^:]*)( ?):( ?)(\'|"){1}([^\}\}\{\{]*)(\'|"){1}( ?)\|( ?)t/U',
      'index'   => 5,
    ],
    'trans_block'  => [
      'pattern'    => '/{%( *)trans( *)%}(.*){%( *)endtrans( *)%}/',
      'index'      => 3,
      'subpattern' => '/{{( *)(\w*)( *)}}/',
    ],
    'php'          => [
      'pattern' => '/( |\(+)t\((\'|")([^\)]*)(\'|")\)/u',
      'index'   => 3,
    ]
  ];

  protected static $phpPatterns = [
    'simple' => [
      'pattern' => '/( |\(+)t\((\'|")([^\)]*)(\'|")\)/u',
      'index'   => 3,
    ]
  ];

  protected static $headers = [
    'Project-Id-Version'        => '',
    'POT-Creation-Date'         => '2017-06-07 18:46+0000\n',
    'PO-Revision-Date'          => 'YYYY-mm-DD HH:MM+ZZZZ\n',
    'Language-Team'             => 'French\n',
    'MIME-Version'              => '1.0\n',
    'Content-Type'              => 'text/plain; charset=utf-8\n',
    'Content-Transfer-Encoding' => '8bit\n',
    'Plural-Forms'              => 'nplurals=2; plural=(n>1);\n',
  ];

  /**
   * Generate module POT file.
   *
   * @param Extension $module
   *   Module object.
   */
  public static function generateModulePoFile(Extension $module) {
    self::parseFiles($module, ['php', 'module'], self::$phpPatterns);
  }

  /**
   * Generate theme POT file.
   *
   * @param Extension $theme
   *   Theme object.
   */
  public static function generateThemePoFile(Extension $theme) {
    self::parseFiles($theme, ['twig', 'theme'], self::$twigPatterns);
  }

  /**
   * Parse file method.
   *
   * @param Extension $module
   *   Module object.
   * @param array $extensions
   *   Files extensions.
   * @param array $patterns
   *   Patterns array.
   */
  private static function parseFiles(Extension $module, array $extensions, array $patterns) {
    $exts = sprintf('(%s)', implode('|', $extensions));
    $files = file_scan_directory(
      $module->getPath(), '/.*\.' . $exts . '$/'
    );
    $translations = [];
    foreach ($files as $file) {
      $contents = file_get_contents($file->uri);

      $translations = array_merge(
        $translations, self::parseFile($contents, $patterns)
      );
    }
    $translations = array_unique($translations);
    $translations = array_map(
      function ($ar) {
        return stripslashes($ar);
      }, $translations
    );

    // On crée le répertoire s'il n'existe pas.
    $dirPath = $module->getPath() . '/translations/';
    if (!is_dir($dirPath)) {
      file_prepare_directory($dirPath, FILE_CREATE_DIRECTORY);
    }

    $outputFile = $module->getPath() . '/translations/' . $module->getName() . '.pot';
    self::generateFile($module, $translations, $outputFile);

  }

  /**
   * Parse specific file.
   *
   * @param string $content
   *   Contents.
   * @param array $patterns
   *   Patterns.
   *
   * @return array
   *   Translations.
   */
  private static function parseFile($content, array $patterns) {
    $translations = [];
    foreach ($patterns as $patternId => $item) {
      $index = $item['index'];
      preg_match_all($item['pattern'], $content, $matches);
      if (!empty($matches[$index])) {
        $matches = $matches[$index];
        if ($patternId === 'trans_block') {
          $matches = array_map(
            function ($elem) use ($item) {
              return preg_replace($item['subpattern'], '@$2', $elem);
            }, $matches
          );
        }
        $translations = array_merge($translations, $matches);
      }
    }
    return ($translations);
  }

  /**
   * Generate file method.
   *
   * @param Extension $module
   *   Module object.
   * @param array $translations
   *   Translations.
   * @param string $outputFile
   *   Output file.
   */
  private static function generateFile(Extension $module, array $translations, $outputFile) {
    self::$headers['Project-Id-Version'] = $module->getName();
    $data = [
      'msgid ""',
      'msgstr ""',
    ];
    foreach (self::$headers as $key => $header) {
      $data[] = "\"$key: $header\"";
    }
    sort($translations);
    foreach ($translations as $line) {
      $data[] = sprintf("\nmsgid \"%s\"", $line);
      $data[] = sprintf("msgstr \"%s\"", $line);
    }
    $contents = implode("\n", $data);

    file_put_contents($outputFile, $contents);

    drush_log(t('Le fichier @output a été créé', ['@output' => $outputFile]), 'ok');

  }

}
