<?php

namespace Drupal\lmwr_tools\Drush;

/**
 * Class LmwrToolsCommandDrush8.
 *
 * Classe tampon pour utiliser le trait.
 * !Attention, si vous déclarez des fonctions ici, elles ne seront pas
 * utilisables en Drush 9.
 * Ajoutez vos fonction dans le LmwrToolsCommandsTrait.
 * Elles seront alors dispo dans les deux versions de drush.
 *
 * @package Drupal\lmwr_tools\Drush
 */
class LmwrToolsCommandsDrush8 {

  const SERVICE_NAME = 'lmwr_tools.commands_drush_8';

  use LmwrToolsCommandsTrait;

}
