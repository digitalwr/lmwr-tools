<?php

namespace Drupal\lmwr_tools\Drush;

use Drupal\Core\Config\NullStorage;
use Drupal\lmwr_tools\Command\ExportCommand;
use Drupal\lmwr_tools\LmwrGeneratePo;

/**
 * Trait LmwrToolsCommandsTrait.
 *
 * Ce trait permet de gérer l'unicité de fonctionnement des commande drush 9 et drush 8.
 *
 * @package Drupal\lmwr_tools\Drush
 */
trait LmwrToolsCommandsTrait {

  /**
   * Create an import from passed .zip file.
   *
   * @option destination
   *   Destination respository.
   * @option remove-hash
   *   Remove config hash.
   * @option without-export
   *   Remove uuid whithout export  configuration.
   * @option export-dev
   *   Export dev.
   * @option remove-uuid
   *   Remove uuid.
   *
   * @command lm-config-export
   *
   * @aliases lmcex
   */
  public function lmConfigExport($options = [
    'destination'    => NULL,
    'remove-hash'    => NULL,
    'without-export' => NULL,
    'export-dev'     => NULL,
    'remove-uuid'    => NULL
  ]) {

    $destination = $options['destination'];
    $doExport = $options['without-export'] != 1;
    $removeHash = $options['remove-hash'] == 1;
    $removeUuid = $options['remove-uuid'] == 1;
    $exportDev = $options['export-dev'] == 1;
    $defaultConfDir = config_get_config_directory(CONFIG_SYNC_DIRECTORY);

    // Init directory.
    if (!$destination) {
      $destination = $defaultConfDir;
    }

    // Do export.
    if ($doExport) {
      // On fait l'export en mode drush 8.
      if (function_exists('drush_invoke')) {
        $args = $defaultConfDir == $destination ? [] : ['--destination=' . $destination];
        drush_invoke('cex', $args);
      }
      else {
        // On fait l'export en mode drush 9.
        /** @var \Drush\Drupal\Commands\config\ConfigExportCommands $exportCommand */
        $exportCommand = \Drupal::service('config.export.commands');
        $exportCommand->export(NULL, $options);
      }
    }

    if ($directory = realpath(DRUPAL_ROOT . '/' . $destination)) {
      // Remove Useless content
      // The two arguments are not used in the process. They're just needed by the constructor.
      $exportCommand = new ExportCommand(
        \Drupal::service('config.manager'), new NullStorage()
      );
      $exportCommand->removeUselessContent($directory, $removeHash, $exportDev, $removeUuid);
    }
    else {
      drush_log(
        'La suppression du contenu n\'a pas été effectuée. Le répertoire de destination n\'est pas correct.',
        'error'
      );
    }
  }

  /**
   * Modifie la version d'un module : Ex: drush smv mon_module 8003.
   *
   * @param string $moduleName
   *   Module name.
   * @param string $version
   *   Version id.
   *
   * @command set_module_version
   *
   * @aliases smv
   */
  public function setModuleVersion($moduleName = NULL, $version = NULL) {
    $doAction = TRUE;
    if (empty($moduleName)) {
      drush_log('Veuillez indiquer le nom du module', 'error');
      $doAction = FALSE;
    }
    if (empty($version)) {
      drush_log('Veuillez indiquer la version à appliquer', 'error');
      $doAction = FALSE;
    }

    if ($doAction) {
      \Drupal::keyValue('system.schema')->set($moduleName, $version);
      $version = \Drupal::keyValue('system.schema')->get($moduleName);
      drush_log(t('Le module @module est désormais en version @version', [
        '@module'  => $moduleName,
        '@version' => $version
      ])->__toString(), 'ok');
    }
    else {
      drush_log('Ex: drush smv mon_module 8003', 'ok');
    }
  }

  /**
   * Génère un point po depuis les sources du module.
   *
   * @param string $type
   *   Type module|theme.
   * @param string $name
   *   Nom de l'élément à traduire.
   *
   * @command generate_po_file
   *
   * @aliases gpo
   */
  public function generatePoFile($type = NULL, $name = NULL) {
    switch ($type) {
      case 'theme':
        $themesInfos = \Drupal::service('theme_handler')->listInfo();
        if (!isset($themesInfos[$name])) {
          drush_log('Theme not found.', 'error');
          return;
        }
        LmwrGeneratePo::generateThemePoFile($themesInfos[$name]);
        break;

      case 'module':
        /** @var \Drupal\Core\Extension\ModuleHandler $moduleService */
        $moduleService = \Drupal::service('module_handler');
        $modules = $moduleService->getModuleList();
        if (!isset($modules[$name])) {
          drush_log('Module not found.', 'error');
          return;
        }
        LmwrGeneratePo::generateModulePoFile($modules[$name]);
        break;
    }
  }

  /**
   * Génère un point po depuis les sources du module.
   *
   * @param string $module
   *   Nom du module.
   *
   * @command reload-module-config
   *
   * @aliases rmc
   */
  public function reloadModuleConfig($module) {
    \Drupal::service('config.installer')
      ->installDefaultConfig('module', $module);
    drush_log(t('Install default config of @module has set.', ['@module' => $module]), 'ok');
  }

}
