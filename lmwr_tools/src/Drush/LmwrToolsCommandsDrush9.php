<?php

namespace Drupal\lmwr_tools\Drush;

use Drush\Commands\DrushCommands;

/**
 * Class LmwrToolsCommandsDrush9.
 *
 * Cette classe est une interface pour Drush 9.
 *
 * @package Drupal\lmwr_tools\Drush
 */
class LmwrToolsCommandsDrush9 extends DrushCommands {

  use LmwrToolsCommandsTrait;

}
