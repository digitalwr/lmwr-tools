<?php

namespace Drupal\lmwr_tools\EventSubscriber;

use Drupal\Core\Entity\EntityInterface;
use Drupal\lmwr_tools\Event\AjaxEntityEvent;
use Drupal\lmwr_tools\LmwrToolsEvents;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class EntitySubscriber.
 *
 * @package lmwr_tools
 */
class EntitySubscriber implements EventSubscriberInterface {

  /**
   * EntitySubscriber constructor.
   */
  public function __construct() {
  }

  /**
   * Returns an array of event names this subscriber wants to listen to.
   *
   * The array keys are event names and the value can be:
   *
   *  * The method name to call (priority defaults to 0)
   *  * An array composed of the method name to call and the priority
   *  * An array of arrays composed of the method names to call and respective
   *    priorities, or 0 if unset
   *
   * For instance:
   *
   *  * array('eventName' => 'methodName')
   *  * array('eventName' => array('methodName', $priority))
   *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
   *
   * @return array
   *   The event names to listen to.
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => 'onKernelRequest',
    ];
  }

  /**
   * Kernel request callback.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   Response event.
   */
  public function onKernelRequest(GetResponseEvent $event) {
    /** @var Request $request */
    $request = $event->getRequest();

    if ($request->isXmlHttpRequest()) {
      $routeName = \Drupal::routeMatch()->getRouteName();
      if (preg_match('/^entity\.(.*)\.canonical$/', $routeName, $matches)) {
        $entityType = $matches[1];
        $routeParameters = \Drupal::routeMatch()->getParameters();

        /** @var EntityInterface $entity */
        $entity = $routeParameters->get($entityType);

        /** @var EventDispatcher $dispatcher */
        $dispatcher = \Drupal::service('event_dispatcher');
        $dispatchEvent = new AjaxEntityEvent($entity, $event);
        $dispatcher->dispatch(LmwrToolsEvents::ENTITY_AJAX_REQUEST, $dispatchEvent);
      }
    }
  }

}
