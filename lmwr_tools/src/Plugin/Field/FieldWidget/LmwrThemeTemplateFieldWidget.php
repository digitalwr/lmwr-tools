<?php

namespace Drupal\lmwr_tools\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\lmwr_tools\Service\Templates;

/**
 * Plugin implementation of the 'lmwr_form_text_editor_format' widget.
 *
 * @FieldWidget(
 *   id = "lmwr_theme_template_field_widget",
 *   label = @Translation("Page template widget"),
 *   field_types = {
 *     "lmwr_theme_template_field_type"
 *   },
 * )
 */
class LmwrThemeTemplateFieldWidget extends WidgetBase {

  /**
   * ID.
   */
  const ID = 'lmwr-page-template-widget';

  /**
   * {@inheritdoc}
   */
  protected function getOptions(FieldableEntityInterface $entity = NULL) {
    /** @var \Drupal\lmwr_tools\Service\Templates $templatesService */
    $templatesService = \Drupal::service(Templates::SERVICE_NAME);

    return ['' => t('- Select -')] + $templatesService->getPageTemplates();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $element += array(
      '#type' => 'select',
      '#default_value' => $value,
      '#options' => $this->getOptions(NULL),
      '#group' => 'page-template',
      '#id' => self::ID,
    );

    return array('value' => $element);
  }

}
