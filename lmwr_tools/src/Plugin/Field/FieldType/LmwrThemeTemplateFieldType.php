<?php

namespace Drupal\lmwr_tools\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Render\Element;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\lmwr_form\Service\LmwrFormPluginManager;
use Drupal\Core\Form\FormState;

/**
 * Provides a field type of baz.
 *
 * @FieldType(
 *   id = "lmwr_theme_template_field_type",
 *   label = @Translation("Page template"),
 *   default_widget = "lmwr_theme_template_field_widget",
 *   default_formatter = "text_default"
 * )
 */
class LmwrThemeTemplateFieldType extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'value' => array(
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string');

    return $properties;
  }

}
