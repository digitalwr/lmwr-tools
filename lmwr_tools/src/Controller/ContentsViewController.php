<?php

namespace Drupal\lmwr_tools\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\lmwr_tools\Service\LanguageService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

/**
 * Class ContentsViewController.
 *
 * Redirect the user to a filter view of the content type passed in parameters.
 *
 * @package Drupal\lmwr_tools\Controller
 */
class ContentsViewController extends ControllerBase {

  /**
   * Redirect to the filtered content view.
   *
   * @return RedirectResponse
   *   The redirect response.
   */
  public function filteredView() {
    $params = [];

    if ($type = \Drupal::routeMatch()->getParameter('bundle')) {
      $langcode = LanguageService::getCurrentLanguageId();

      $params['query']['langcode'] = $langcode;
      $params['query']['type'] = explode('|', $type);
    }

    $url = Url::fromRoute('system.admin_content', [], $params);

    return RedirectResponse::create($url->toString());

  }

}
