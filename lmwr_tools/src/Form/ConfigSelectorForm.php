<?php

namespace Drupal\lmwr_tools\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\lmwr_tools\Service\ConfigSelectorService;

/**
 * Class ConfigSelectorForm.
 *
 * @package Drupal\lmwr_tools\Form
 */
class ConfigSelectorForm extends FormBase {

  const FORM_ID = 'lmwr_tools.config_selector_form';

  const FIELD_REPO = 'repos';
  const FIELD_CREATE_REPO = 'create_repo';
  const FIELD_CONF_TO_EXPORT = 'conf_to_export';

  /**
   * The config selector service.
   *
   * @var ConfigSelectorService
   */
  protected $csService;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->csService = \Drupal::service(ConfigSelectorService::SERVICE_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return static::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'lmwr_tools/conf_selector';

    // Destination fieldset.
    $form['destination'] = [
      '#type'  => 'fieldset',
      '#title' => t('Destination'),
    ];

    // Existing elements.
    $existingRepos = $this->csService->getExistingPartialRepos()
      + [static::FIELD_CREATE_REPO => t('Créer un nouveau répertoire')];

    $form['destination'][static::FIELD_REPO] = [
      '#type'          => 'select',
      '#title'         => t('Répertoire de destination'),
      '#default_value' => reset($existingRepos),
      '#options'       => $existingRepos,
      '#ajax'          => [
        'callback' => '::onAjaxRepoChange',
        'event'    => 'change',
        'wrapper'  => 'checkbox-wrapper',
      ],
    ];

    $form['destination'][static::FIELD_CREATE_REPO] = [
      '#type'   => 'textfield',
      '#title'  => t('Nom du répertoire'),
      '#states' => [
        'required' =>
          [':input[name="' . static::FIELD_REPO . '"]' => ['value' => static::FIELD_CREATE_REPO]],
        'visible'  =>
          [':input[name="' . static::FIELD_REPO . '"]' => ['value' => static::FIELD_CREATE_REPO]],
      ]
    ];

    // Init checkboxes.
    $this->initCheckBoxes($form, $form_state);

    $form['submit'] = [
      '#type'        => 'submit',
      '#value'       => t('Export'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Refresh selection on repo change.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return array
   *   THe result.
   */
  public function onAjaxRepoChange(array &$form, FormStateInterface $formState) {
    return $form['checkboxes'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $confToExport = $this->getCleanConfToExport($formState);

    $this->csService->exportSelection($confToExport, $this->getCurrentRepoName($form, $formState));
  }

  /**
   * Init checkboxes selection.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form State.
   */
  protected function initCheckBoxes(array &$form, FormStateInterface $formState) {
    $allConfig = $this->csService->getAllCurrentConfig();
    $existingConfig = $this->csService->getExistingConfigInRepo($this->getCurrentRepoName($form, $formState));

    $form['checkboxes'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => ['checkbox_groups']
      ],
      '#prefix'     => '<div id="checkbox-wrapper">',
      '#suffix'     => '</div>',
      'children'    => []
    ];

    // For each conf name.
    $i = 0;
    $groupName = '';
    do {
      $confName = $allConfig[$i];
      $confNameData = explode('.', $confName);

      // Create group fieldset if group is different than previous iteration.
      if ($confNameData[0] != $groupName) {
        $groupName = $confNameData[0];
        $this->createGroupFieldset($form, $groupName);
      }

      // Add checkboxes in fieldset.
      $this->addCheckbox($form, $confName, $groupName, is_array($existingConfig) && in_array($confName, $existingConfig));

      $i++;
    } while ($i < count($allConfig));
  }

  /**
   * Create group fieldset.
   *
   * @param array $form
   *   The form.
   * @param string $groupName
   *   The group Name.
   */
  protected function createGroupFieldset(array &$form, $groupName) {
    // Init text field.
    $form['checkboxes']['children'][$groupName] = [
      '#type'  => 'fieldset',
      '#title' => t(ucfirst($groupName)),
      '#open'  => FALSE
    ];
  }

  /**
   * Add checkbox.
   *
   * @param array $form
   *   THe form.
   * @param string $confName
   *   THe ConfNAme.
   * @param string $groupName
   *   The group Name.
   * @param bool $checked
   *   The status.
   */
  protected function addCheckbox(array &$form, $confName, $groupName, $checked) {

    $form['checkboxes']['children'][$groupName][str_replace('.', '-', $confName)] = [
      '#type'  => 'checkbox',
      '#title' => t($confName),
      '#value' => $checked,
    ];
  }

  /**
   * Return the current selected repo name.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The formstate.
   *
   * @return string
   *   The name.
   */
  protected function getCurrentRepoName(array $form, FormStateInterface $formState) {
    $values = $formState->getValues();
    // Submission state.
    if (!empty($values)) {
      if ($formState->getValue(static::FIELD_REPO) === static::FIELD_CREATE_REPO) {
        return $formState->getValue(static::FIELD_CREATE_REPO) . '';
      }
      return $formState->getValue(static::FIELD_REPO) . '';
    }
    // Initial state.
    else {
      $destination = $form['destination'];
      if ($destination[static::FIELD_REPO]['#default_value'] !== static::FIELD_CREATE_REPO) {
        return $destination[static::FIELD_REPO]['#default_value'] . '';
      }
    }

    return '';

  }

  /**
   * Return the list of conf to export.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The formstate.
   *
   * @return array
   *   The conf to export.
   */
  protected function getCleanConfToExport(FormStateInterface $formState) {
    $allConf = $this->csService->getAllCurrentConfig();
    $userInputs = $formState->getUserInput();
    return array_filter($allConf, function ($confName) use ($formState, $userInputs) {
      $key = str_replace('.', '-', $confName);
      return array_key_exists($key, $userInputs) && $userInputs[$key];
    });
  }

}
