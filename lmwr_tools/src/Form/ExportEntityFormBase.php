<?php

namespace Drupal\lmwr_tools\Form;

use Drupal\Core\Config\Entity\Query\Query;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\lmwr_tools\Shared\BatchTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Drupal\Core\Url;

/**
 * Class ExportEntityFormBase.
 *
 * @package Drupal\lmwr_tools\Form
 */
abstract class ExportEntityFormBase extends FormBase {

  use BatchTrait;
  use StringTranslationTrait;

  /**
   * Date fieldset name.
   */
  const FIELDSET_DATE = 'date';

  /**
   * Date from field name.
   */
  const FIELD_DATE_FROM = 'date_from';

  /**
   * Date to field name.
   */
  const FIELD_DATE_TO = 'date_to';

  /*
   * Submit name.
   */
  const FIELD_SUBMIT = 'export';

  /**
   * CSV default delimiter.
   */
  const CSV_DELIMITER = ';';

  /**
   * CSV Inner delimiter.
   */
  const CSV_INNER_DELIMITER = ' , ';

  /**
   * Key base for specific storage.
   */
  const KEY_BASE = 'lmwr_tools.export_base';

  /**
   * Key for filename recovery.
   */
  const KEY_FILE_NAME = 'filename';

  /**
   * Number of entities treated in a batch operation.
   */
  const NUMBER_OF_ENTITIES_PER_BATCH_OPERATION = 1;

  /**
   * CSV Header map.
   *
   * @var array
   */
  protected $csvHeader;

  /**
   * The file generated.
   *
   * @var resource
   */
  protected $file;

  /**
   * Process identifier.
   *
   * @var string
   */
  protected $currentProcess;

  /**
   * Return the entity type manager of the entity type to export.
   *
   * @return string
   *   The entity type id.
   */
  abstract public function getEntityTypeId();

  /**
   * Return the readable name.
   *
   * @return string
   *   A readable form name.
   */
  abstract public function getName();

  /**
   * Return the BOM char that indicates utf8 to excel.
   *
   * @return string
   *   The bom chr.
   */
  public static function getBomChar() {
    return chr(239) . chr(187) . chr(191);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Download file if has been processed.
    $this->initDownloadProcess($form);

    // Date fieldset.
    $form[self::FIELDSET_DATE] = [
      '#type'  => 'fieldset',
      '#title' => 'Exporter les données',
    ];

    if ($this->getLatestExportTime()) {
      $form[self::FIELDSET_DATE]['info'] = [
        '#markup' => '<div><strong>'
          . $this->t('Le dernier élément exporté date du ')
          . \Drupal::service('date.formatter')
            ->format($this->getLatestExportTime(), 'short')
          . '</strong></div>'
      ];
    }

    // Date from.
    $form[self::FIELDSET_DATE][self::FIELD_DATE_FROM] = [
      '#type'          => 'datetime',
      '#title'         => 'Du (inclus): ',
      '#default_value' => $this->getLatestExportTime() ? DrupalDateTime::createFromTimestamp($this->getLatestExportTime() + 1) : NULL,
    ];

    // Date to.
    $form[self::FIELDSET_DATE][self::FIELD_DATE_TO] = [
      '#type'          => 'datetime',
      '#title'         => 'Au (inclus): ',
      '#default_value' => DrupalDateTime::createFromTimestamp(time()),
    ];

    // Submit.
    $form['actions'][self::FIELD_SUBMIT] = [
      '#type'       => 'submit',
      '#value'      => 'Télécharger le .csv',
      '#attributes' =>
        [
          'class' => ['button--primary']
        ]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get the list of entity ids to export.
    $entitiesToExport = $this->getIdsToExport($form, $form_state);

    if (count($entitiesToExport) > 0) {
      // Define identifier for the current file to download.
      $this->currentProcess = time();

      // Init file.
      $this->initFile();

      // Header.
      $this->initDefaultHeader();
      $this->writeHeader();

      // Add data to process list.
      $entitiesToExport = array_map(array(
        $this,
        'prepareDataToBatch'
      ), $entitiesToExport);

      // Generate operations for batch.
      $operations = $this->getBatchOperations(
        '\\' . get_called_class() . '::processListBatch',
        $entitiesToExport,
        static::NUMBER_OF_ENTITIES_PER_BATCH_OPERATION
      );

      // Launch batch.
      $batch = array(
        'title'      => $this->getName(),
        'operations' => $operations,
        'finished'   => '\\' . get_called_class() . '::processEnd',
      );
      batch_set($batch);
    }
    else {
      \Drupal::messenger()
        ->addError($this->t('Pas d\'élément à exporter pour cette période'));
    }
  }

  /**
   * Get the tempstore object.
   *
   * @return mixed
   *   The current tempstore object.
   */
  public function getTempstore() {
    return \Drupal::service('user.private_tempstore')
      ->get(static::KEY_BASE . '.' . $this->getFormId());
  }

  /**
   * Return the file to download name.
   *
   * @return string|null
   *   The current file name to download.
   */
  protected function getWaitingFileName() {
    return $this->getTempstore()->get(static::KEY_FILE_NAME);
  }

  /**
   * Download the file.
   *
   * If the form is in display view, the method add an iframe with the src
   * to the download page.
   * If the form is in download page mode (according to url parameters), we
   * download the file.
   *
   * @param array $form
   *   The current form.
   */
  protected function initDownloadProcess(array &$form) {
    // Donwload process.
    if ($downloadFilename = \Drupal::request()->get(static::KEY_FILE_NAME)) {
      $this->downloadFile($downloadFilename);
    }

    // Generate download link.
    if ($waitingFileName = $this->getWaitingFileName()) {
      $url = Url::fromRoute(\Drupal::routeMatch()
        ->getRouteName(), [static::KEY_FILE_NAME => $waitingFileName])
        ->toString();

      $form['download_frame'] = [
        '#markup'       => '<iframe class="visually-hidden"  src="' . $url . '"></iframe>',
        '#allowed_tags' => ['iframe'],
        '#cache'        => FALSE
      ];
    }
  }

  /**
   * Batch operation adding line to file.
   *
   * @param array $list
   *   Elements to treat.
   * @param array $context
   *   Context.
   */
  static public function processListBatch(array $list, array &$context) {
    // Launch process.
    $form = reset($list)['form'];
    $form->generateFile($list);

    $context['results']['form'] = $form;
  }

  /**
   * Method called at the end of the batch.
   *
   * @param bool $success
   *   The batch process status.
   * @param array $results
   *   The result data.
   * @param array $operations
   *   The operations.
   */
  static public function processEnd($success, array $results, array $operations = []) {
    if ($success) {
      // Save file name in session for donwload after redirect.
      $form = $results['form'];
      $form->getTempstore()->set(static::KEY_FILE_NAME, $form->getFileName());

      \Drupal::messenger()
        ->addStatus(t('Les données ont été correctement exportées'));
    }
    else {
      \Drupal::messenger()
        ->addError(t('Les données ont été correctement exportées'));
    }
  }

  /**
   * Add elements to export to the file.
   *
   * @param array $list
   *   List of elements to treat.
   */
  public function generateFile(array $list) {
    $this->initFile();

    // Get entities from list.
    $entitiesToExport = array_map(array($this, 'getEntityFromListItem'), $list);

    // Write elements rows.
    foreach ($entitiesToExport as $key => $entity) {
      $this->writeRow($this->getEntityRow($entity));
    }

    // Save latest elements creation date.
    $this->setLatestExportTime($entity->get('created')->first()->value);

    fclose($this->file);
  }

  /**
   * Download the generated file.
   *
   * @param string $fileName
   *   The filename.
   */
  public function downloadFile($fileName) {
    if (file_exists(file_directory_temp() . '/' . $fileName)) {
      $response = new Response(file_get_contents(file_directory_temp() . '/' . $fileName));
      $disposition = $response->headers->makeDisposition(
        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
        $fileName
      );
      $response->headers->set('Content-Disposition', $disposition);

      // Delete file after download.
      unlink(file_directory_temp() . '/' . $fileName);
      $this->getTempstore()->set(static::KEY_FILE_NAME, NULL);

      $response->send();
      exit;
    }
  }

  /**
   * Add the form to each element to treat in order to get it in the operations.
   *
   * @param int $data
   *   The entity ID.
   *
   * @return array
   *   The array of data passed to opeartions.
   */
  protected function prepareDataToBatch($data) {
    return [
      'id'   => $data,
      'form' => $this
    ];
  }

  /**
   * Return the entity from its ID.
   *
   * @param int $item
   *   The entity ID.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity.
   */
  protected function getEntityFromListItem($item) {
    return $this->getEntityStorage()->load($item['id']);
  }

  /**
   * Open the generated file in the tmp directory.
   */
  protected function initFile() {
    $tmpFilePath = file_directory_temp() . '/' . $this->getFileName();
    $doAddBom = !file_exists($tmpFilePath);
    $this->file = fopen($tmpFilePath, 'a+');

    if ($doAddBom) {
      // BOM for excel UTF8.
      fwrite($this->file, static::getBomChar());
    }
  }

  /**
   * Write the header row.
   */
  protected function writeHeader() {
    $this->writeRow($this->csvHeader);
  }

  /**
   * Write a row.
   *
   * @param array $data
   *   Data to write.
   */
  public function writeRow(array $data) {
    fputcsv($this->file, $data, self::CSV_DELIMITER);
  }

  /**
   * Get the row to write for the entity passed.
   *
   * It allows you to use specific method for specific fields :
   * exportField{Field_name}( $entity ) Ex. exportFieldField_name($entity).
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to write.
   *
   * @return array
   *   The data to write.
   */
  protected function getEntityRow(EntityInterface $entity) {
    $row = [];
    foreach (array_keys($this->csvHeader) as $col) {

      // Specific method.
      $method = 'exportField' . ucfirst($col);
      if (method_exists($this, $method)) {
        $row[$col] = $this->$method($entity, $col);
      }
      else {
        $row[$col] = $this->getFieldColumn($entity->get($col));
      }
    }

    return $row;
  }

  /**
   * Return the value of a field to write.
   *
   * @param \Drupal\Core\Field\FieldItemList $data
   *   The data to write.
   *
   * @return string
   *   The string to export.
   */
  protected function getFieldColumn(FieldItemList $data) {
    if ($data->count() > 1) {
      return implode(self::CSV_INNER_DELIMITER, array_map(array(
        $this,
        'getMultipleValue'
      ), $data->getValue()));
    }
    else {
      if ($field = $data->first()) {
        return $field->value;
      }
    }
    return '';
  }

  /**
   * Return the value of a FieldListItem.
   *
   * @param array $data
   *   The field list item.
   *
   * @return string
   *   The string to value.
   */
  protected function getMultipleValue(array $data) {
    return $data['value'];
  }

  /**
   * Return the ids list to export.
   *
   * @param array $form
   *   The Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   List of ids to export.
   */
  protected function getIdsToExport(array &$form, FormStateInterface $form_state) {
    $query = $this->getDefaultQuery($form, $form_state);

    if ($result = $query->execute()) {
      return $result;
    }
    return [];
  }

  /**
   * Return the default query with date and sort.
   *
   * @param array $form
   *   The Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return Query
   *   The default query invoked for getting the ids.
   */
  protected function getDefaultQuery(array &$form, FormStateInterface $form_state) {
    $query = \Drupal::entityQuery($this->getEntityTypeId());

    /** @var DrupalDateTime $date */
    if ($dateFrom = $form_state->getValue(self::FIELD_DATE_FROM)) {
      $query->condition('created', strtotime($dateFrom . ''), '>=');
    }
    /** @var DrupalDateTime $date */
    if ($dateTo = $form_state->getValue(self::FIELD_DATE_TO)) {
      $query->condition('created', strtotime($dateTo . ''), '<=');
    }

    if ($this->getEntityBundle() != $this->getEntityTypeId()) {
      $query->condition('type', $this->getEntityBundle());
    }
    $query->sort('created', 'ASC');
    return $query;
  }

  /**
   * Return the last exported element creation date.
   *
   * @return string|null
   *   The last exported element creation date.
   */
  protected function getLatestExportTime() {

    return \Drupal::state()->get($this->getlastExportedDateKey());
  }

  /**
   * Init the last exported element creation date.
   *
   * @param string $date
   *   The timestamp of the last exported element creation date.
   */
  protected function setLatestExportTime($date) {
    \Drupal::state()->set($this->getlastExportedDateKey(), $date);
  }

  /**
   * Init the header column map.
   */
  protected function initDefaultHeader() {
    $this->csvHeader = [];
    $fields = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions($this->getEntityTypeId(), $this->getEntityBundle());

    /** @var FieldConfig $data */
    foreach ($fields as $fieldName => $data) {
      $this->csvHeader[$fieldName] = $data->getLabel() . '';
    }
  }

  /**
   * Get the Entity type storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The Entity type storage.
   */
  public function getEntityStorage() {
    return \Drupal::entityTypeManager()->getStorage($this->getEntityTypeId());
  }

  /**
   * Get the current process file name.
   *
   * @return string
   *   The current process file name.
   */
  public function getFileName() {
    return $this->getEntityTypeId() . '_' . $this->currentProcess . '.csv';
  }

  /**
   * Get the entity bundle to export.
   *
   * @return string
   *   The entity bundle to export.
   */
  protected function getEntityBundle() {
    return $this->getEntityTypeId();
  }

  /**
   * Get the key to retrieve the last exported date.
   *
   * @return string
   *   The last exported date key.
   */
  protected function getlastExportedDateKey() {
    return static::KEY_BASE . '.last_export_date.' . $this->getEntityTypeId();
  }

  /**
   * Get the readable creation date.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $fieldName
   *   THe field name.
   *
   * @return string
   *   The date.
   */
  protected function exportFieldCreated(EntityInterface $entity, $fieldName) {
    return $this->getReadableDate($entity->get($fieldName)->first()->value);
  }

  /**
   * Get the readable creation date.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $fieldName
   *   THe field name.
   *
   * @return string
   *   The date.
   */
  protected function exportFieldChanged(EntityInterface $entity, $fieldName) {
    return $this->getReadableDate($entity->get($fieldName)->first()->value);
  }

  /**
   * Turn timestamp into readable short date.
   *
   * @param string $timestamp
   *   Timestamp.
   * @param string $format
   *   Format.
   *
   * @return mixed
   *   Return a readable date
   */
  protected function getReadableDate($timestamp, $format = 'short') {
    return \Drupal::service('date.formatter')
      ->format($timestamp, $format);
  }

}
