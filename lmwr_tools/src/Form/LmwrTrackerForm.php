<?php

namespace Drupal\lmwr_tools\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\lmwr_tools\Service\Tracking;

/**
 * TrackerForm class.
 */
class LmwrTrackerForm extends FormBase {

  /**
   * Get form ID method.
   *
   * @return string
   *   Form ID.
   */
  public function getFormId() {
    return 'lmwr_tracker_form';
  }

  /**
   * Build form method.
   *
   * @return array
   *   Build array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($list = Tracking::me()->getList()) {
      $form['project_id'] = [
        '#type' => 'select',
        '#required' => TRUE,
        '#title' => t('Project'),
        '#empty_option' => t('- Select -'),
        '#default_value' => Tracking::me()->getCurrentProjectId() ?: '',
        '#options' => [],
      ];

      foreach ($list as $item) {
        $form['project_id']['#options'][$item->id] = $item->name;
      }

      $form['actions'] = ['#type' => 'actions'];

      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit'),
      ];
    }
    else {
      drupal_set_message(t('Websites data can not be loaded from %url', ['%url' => Tracking::ENDPOINT]), 'error');
    }

    return $form;
  }

  /**
   * Submit form method.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    Tracking::me()->setProjectId($form_state->getValue('project_id'));
    drupal_set_message(t('Your configuration has been saved.'));
  }

}
