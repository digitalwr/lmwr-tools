<?php

namespace Drupal\lmwr_tools\Form;

use Cocur\Slugify\Slugify;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\Entity\BaseFieldOverride;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\language\Plugin\migrate\source\d6\LanguageContentSettings;
use Drupal\migrate\Plugin\migrate\destination\EntityFieldStorageConfig;

/**
 * Class DuplicateConfigEntityTypeForm.
 *
 * @package Drupal\lmwr_tools\Form
 */
class DuplicateConfigEntityTypeForm extends FormBase {
  const FORM_ID = 'lmwr_tools.duplicate_config_entity_type';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return self::FORM_ID;
  }

  /**
   * Return le bundle initial.
   *
   * @return \Drupal\Core\Config\Entity\ConfigEntityBase
   *   The current config entity base to duplicate.
   */
  protected function getInitialBundle() {
    /** @var \Drupal\Core\Config\Entity\ConfigEntityStorage $confEntityStorage */
    $confEntityStorage = \Drupal::entityTypeManager()
      ->getStorage($this->getType());

    /** @var \Drupal\Core\Config\Entity\ConfigEntityType $configEntityType */
    $configEntityType = $confEntityStorage->getEntityType();

    return call_user_func($configEntityType->getClass() . '::load', \Drupal::routeMatch()
      ->getParameter('initial_bundle'));
  }

  /**
   * Retourne le type courant de l'entité config.
   *
   * @return string
   *   Le nom du type.
   */
  protected function getType() {
    return \Drupal::routeMatch()->getParameter('type');
  }

  /**
   * Retourne le type d'entité lié.
   *
   * @return string
   *   Le type d'entité lié à la conf.
   */
  protected function getEntityTypeId() {
    return $this->getInitialBundle()->getEntityType()
      ->getBundleOf();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $previous = $this->getInitialBundle();

    $form['title']['#markup'] = '<h2>' . t('Duplicate @initial', ['@initial' => $previous->label()]) . '</h2>';

    $form['name'] = [
      '#type'          => 'textfield',
      '#title'         => t('Nom'),
      '#default_value' => t($previous->label()),
      '#required'      => TRUE,
    ];

    $form['slug'] = [
      '#type'     => 'textfield',
      '#title'    => t('Nom machine'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type'        => 'submit',
      '#value'       => t('Duplicate'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $initialBundle = $this->getInitialBundle();
    $newBundle = $initialBundle->createDuplicate();

    // Initialisation du nouveau bundle.
    $slugifier = new Slugify();
    $slug = str_replace('-', '_', $slugifier->slugify($form_state->getValue('slug')));
    $keys = $newBundle->getEntityType()->getKeys();
    $labelKey = $keys['label'];
    $idKey = $keys['id'];
    $newBundle->set($idKey, $slug);
    $newBundle->set($labelKey, $form_state->getValue('name'));
    $newBundle->save();

    $errors = [];

    // Save fields.
    try {
      $this->saveFields($newBundle, $initialBundle);
    }
    catch (\Exception $e) {
      $errors[] = t('La duplication n\'a pas abouti : @message', ['@message' => $e->getMessage()]);
    }

    if (empty($errors)) {

      // Gestion des traductions.
      try {
        $this->initTranslation($newBundle, $initialBundle);
      }
      catch (\Exception $e) {
        $errors[] = t('La configuration des traductions n\'a pas abouti : @message', ['@message' => $e->getMessage()]);
      }

      // Save form display.
      try {
        $this->saveFormDisplay($initialBundle, $newBundle);
      }
      catch (\Exception $e) {
        $errors[] = t('La configuration du formulaire n\'a pas abouti : @message', ['@message' => $e->getMessage()]);
      }

      // Save view display.
      try {
        $this->saveViewDisplay($initialBundle, $newBundle);
      }
      catch (\Exception $e) {
        $errors[] = t('La configuration de l\'affichage n\'a pas abouti : @message', ['@message' => $e->getMessage()]);
      }
    }

    // Affichage des erreurs.
    if (!empty($errors)) {
      foreach ($errors as $error) {
        \Drupal::messenger()->addError($error);
      }
    }

    // Redirection.
    $form_state->setRedirectUrl(Url::fromRoute(\Drupal::routeMatch()
      ->getParameter('redirect')));
  }

  /**
   * Génère la configuration des fields du nouveau type d'entité.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityBase $newBundle
   *   Le nouveau type.
   * @param \Drupal\Core\Config\Entity\ConfigEntityBase $initialBundle
   *   Le type dupliqué.
   */
  protected function saveFields(ConfigEntityBase $newBundle, ConfigEntityBase $initialBundle) {
    $initialFields = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions($this->getEntityTypeId(), $initialBundle->id());

    $newFields = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions($this->getEntityTypeId(), $newBundle->id());

    foreach ($initialFields as $fieldName => $field) {
      if ($field_storage = FieldStorageConfig::loadByName($this->getEntityTypeId(), $fieldName)) {
        $this->duplicateField($newBundle, $fieldName, $field);
      }
      elseif ($field instanceof BaseFieldOverride) {
        $this->duplicateProperty($newBundle, $initialBundle, $newFields, $initialFields, $field);
      }
    }
  }

  /**
   * Duplicate fields.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityBase $newBundle
   *   THe new config entity base.
   * @param string $fieldName
   *   THe fieldName.
   * @param \Drupal\field\Entity\FieldConfig $initialField
   *   The initial field.
   */
  protected function duplicateField(ConfigEntityBase $newBundle, $fieldName, FieldConfig $initialField) {

    $field_storage = FieldStorageConfig::loadByName($this->getEntityTypeId(), $fieldName);

    $field = FieldConfig::loadByName($this->getEntityTypeId(), $newBundle->id(), $fieldName);

    // Initialisation des données.
    $data = $initialField->toArray();
    unset($data['id']);
    $data['uuid'] = \Drupal::service('uuid')->generate();
    $data['field_storage'] = $field_storage;
    $data['bundle'] = $newBundle->id();
    $data['label'] = $initialField->label();

    if (empty($field)) {
      $fieldConfig = FieldConfig::create($data);
      $fieldConfig->save();
    }

  }

  /**
   * Duplique la configuration des propriétés du type d'entité.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityBase $newBundle
   *   Le nouveau type d'entité.
   * @param \Drupal\Core\Config\Entity\ConfigEntityBase $initialBundle
   *   Le type dupliqué.
   * @param array $newFields
   *   La liste des fields du nouveau type.
   * @param array $initialFields
   *   La liste des fields du type dupliqué.
   * @param BaseFieldOverride $field
   *   Le field à dupliquer.
   */
  protected function duplicateProperty(ConfigEntityBase $newBundle, ConfigEntityBase $initialBundle, array $newFields, array $initialFields, BaseFieldOverride $field) {
    $initialConfName = 'core.base_field_override.' . $this->getEntityTypeId() . '.' . $initialBundle->id() . '.' . $field->getName();
    $newConfName = 'core.base_field_override.' . $this->getEntityTypeId() . '.' . $newBundle->id() . '.' . $field->getName();

    $initialConfig = \Drupal::config($initialConfName)->getRawData();
    if (!empty($initialConfig)) {
      $data = $initialConfig;
      $data['uuid'] = \Drupal::service('uuid')->generate();
      $data['id'] = $this->getEntityTypeId() . '.' . $newBundle->id() . '.' . $field->getName();
      $data['bundle'] = $newBundle->id();

      \Drupal::configFactory()
        ->getEditable($newConfName)
        ->setData($data)
        ->save();
    }
  }

  /**
   * Init form type.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityBase $initialBundle
   *   The initial config entity base.
   * @param \Drupal\Core\Config\Entity\ConfigEntityBase $newBundle
   *   The new new config entity base.
   */
  protected function saveFormDisplay(ConfigEntityBase $initialBundle, ConfigEntityBase $newBundle) {
    $query = \Drupal::database()->select('config');
    $query->addField('config', 'name');
    $query->condition('name', 'core.entity_form_display.' . $initialBundle->getEntityType()
      ->getBundleOf() . '.' . $initialBundle->id() . '%', 'like');

    foreach ($query->execute()->fetchCol() as $displayType) {
      $displayType = explode('.', $displayType);
      $displayType = end($displayType);
      /** @var \Drupal\Core\Entity\Entity\EntityFormDisplay $initialFormDisplay */
      $initialFormDisplay = $form_display = \Drupal::service('entity_type.manager')
        ->getStorage('entity_form_display')
        ->load($this->getEntityTypeId() . '.' . $initialBundle->id() . '.' . $displayType);

      /** @var \Drupal\Core\Entity\Entity\EntityFormDisplay $newFormDisplay */
      $newFormDisplay = $initialFormDisplay->createDuplicate();
      $newFormDisplay->set('bundle', $newBundle->id());
      $newFormDisplay->save();
    }

  }

  /**
   * Init display type.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityBase $initialBundle
   *   THe initial new config entity base.
   * @param \Drupal\Core\Config\Entity\ConfigEntityBase $newBundle
   *   The new config entity base.
   */
  protected function saveViewDisplay(ConfigEntityBase $initialBundle, ConfigEntityBase $newBundle) {
    $query = \Drupal::database()->select('config');
    $query->addField('config', 'name');
    $query->condition('name', 'core.entity_view_display.' . $initialBundle->getEntityType()
      ->getBundleOf() . '.' . $initialBundle->id() . '%', 'like');

    foreach ($query->execute()->fetchCol() as $displayType) {
      $displayType = explode('.', $displayType);
      $displayType = end($displayType);
      /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay $initialViewDisplay */
      $initialViewDisplay = $form_display = \Drupal::service('entity_type.manager')
        ->getStorage('entity_view_display')
        ->load($this->getEntityTypeId() . '.' . $initialBundle->id() . '.' . $displayType);

      /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay $newViewDisplay */
      $newViewDisplay = $initialViewDisplay->createDuplicate();
      $newViewDisplay->set('bundle', $newBundle->id());
      $newViewDisplay->save();
    }

  }

  /**
   * Génère la configuration de la traduction.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityBase $newBundle
   *   Le nouveau type.
   * @param \Drupal\Core\Config\Entity\ConfigEntityBase $initialBundle
   *   Le type dupliqué.
   */
  protected function initTranslation(ConfigEntityBase $newBundle, ConfigEntityBase $initialBundle) {
    if ($initialLanguageConf = \Drupal::entityTypeManager()
      ->getStorage('language_content_settings')
      ->load($this->getEntityTypeId() . '.' . $initialBundle->id())) {
      $data = $initialLanguageConf->toArray();
      $data['uuid'] = \Drupal::service('uuid')->generate();
      $data['id'] = $this->getEntityTypeId() . '.' . $newBundle->id();
      $data['target_bundle'] = $newBundle->id();

      $newLanguageConf = \Drupal::entityTypeManager()
        ->getStorage('language_content_settings')
        ->create($data);
      $newLanguageConf->save();
    }
  }

}
