#Export Entity Form Base
## Introduction
The ExportEntityFormBase is an abstract class alowing you to implement an export 
form that export a list of data (by default whatever entity data) in a file, using a batch process.
The basic process :
 1. buildForm :
    1. Generate a form with two date fields allowing to restrict the data to export according to the entity creation date. 
    (By default its prepopulated by the creation date of the last entity exported)
    2. If the form is displayed after the batch process and a file has been generated and is waiting to be downloaded, 
     the form display a hidden iframe to the file downloaded page (initDownloadProcess).
   
 2. submitForm :
    1. Get the list of elements to export (getIdsToExport)
    2. Generate a header map array with [{$field_id}=>{$readable_ame}] (initDefaultHeader)
    3. Prepare the file to download in the tmp directory
    4. Transform each elements to export into a treatable array sent to the batch process (prepareDataToBatch)
    5. Init and launch the batch process
   
 3. The batch process: 
    1. Each operation of the batch is processed in a static function (processListBatch). 
    Each operation writes a line in the result file.
    2. At the end of the process the file is not downloaded directly in order to keep the standard redirecion.
    The file name is stocked in the user session, and then recovered and displayed in an iframe when the initial form is 
    re-displayed after the batch ending process redirection.
     
 
## The abstract method : 
 * getEntityTypeId : must return the entity to export type id.
 * getName : must return a readable name, displayed during the batch process
 
 
## How to ?
 ### Set the list of columns.
 You can set the list of columns in your csv overriding the initDefaultHeader function. This function init the $csvHeader 
 property which is an associative map with :  [{$col_id}=>{$readable_ame}]
 
 ### Set the render for a specific column:
  For each treated row, the process is looking for a specific function called "exportField{Col_id}(Entity $entity, $colName)".
  You can see the example of "exportFieldCreated" and "exportFieldChanged"
  
 ### Set the export file type.
 The default process exports a csv, but in theory (not tested yet), you can use a different export format keeping the EntityExportFormBase process.
 For this you will have to override several functions : 
 - initFile : create the file.
 - getFileName : return the generated file name.
 - writeHeader : write the header csv row. 
 - getEntityRow : return the array of data of one entity to be exported. 
 - writeRow : write the exported element in the generated file.
 - processEnd : override this with caution if you want to alter the generated file after the all rows generation. 
 For example, use this function if you kwant to close an xml node or other closure elements.
 
 ### Set the number of entities generation per batch operation
 By default, each batch operation treats one entity, but you can treat several entities by operation. For this
 you have to override the NUMBER_OF_ENTITIES_PER_BATCH_OPERATION constant.
 
 
  