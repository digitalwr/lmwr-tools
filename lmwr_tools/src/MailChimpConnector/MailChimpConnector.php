<?php

namespace Drupal\lmwr_tools\MailChimpConnector;

use \DrewM\MailChimp\MailChimp;

/**
 * Class MailChimp.
 *
 * @package Drupal\lmwr_tools\Service
 */
class MailChimpConnector {

  /**
   * Key of the Mailchimp API.
   *
   * @var string
   */
  private $apiKey;

  /**
   * Technical ID of the MailChimp list.
   *
   * @var string
   */
  private $listId;

  /**
   * MailChimp service.
   *
   * @var MailChimp
   */
  private $mc;

  /**
   * Constructor.
   *
   * @param string $apiKey
   *   The key of the MailChimp API.
   *
   * @throws \Exception
   *   Invalid parameters.
   */
  public function __construct($apiKey) {
    if (!is_string($apiKey)) {
      throw new \Exception('Api Key must be string');
    }

    $this->apiKey = $apiKey;

    // Par défault on utilise la classe DrewM\MailChimp\MailChimp.
    $this->mc = new MailChimp($this->apiKey);
  }

  /**
   * Utilisation de proxy.
   *
   * @param string $proxy_host
   *   Proxy host.
   * @param string $proxy_port
   *   Proxy port.
   */
  public function setProxy($proxy_host = NULL, $proxy_port = NULL) {
    if (!empty($proxy_host) && !empty($proxy_port)) {
      if ($this->mc instanceof MailChimpWithProxy) {
        $this->mc->setProxy($proxy_host, $proxy_port);
      }
      else {
        $this->mc = new MailChimpWithProxy($this->apiKey, NULL, $proxy_host, $proxy_port);
      }
    }
  }

  /**
   * Return the listId.
   *
   * @return string
   *   The list id.
   */
  public function getListId() {
    return $this->listId;
  }

  /**
   * Define the listId used.
   *
   * @param string $listId
   *   The list id.
   */
  public function setListId($listId) {
    $this->listId = $listId;
  }

  /**
   * Subscribe in MailChimp list.
   *
   * @param string $email
   *   L'email.
   * @param string|null $listId
   *   L'id de la liste.
   * @param string $status
   *   Le status.
   * @param array $mergeVars
   *   Le tableau d'option mergeVars.
   *
   * @return bool
   *   Retourne vrai si l'envoi a été effectué.
   *
   * @throws \Exception
   */
  public function subscribe($email, $listId = NULL, $status = 'subscribed', array $mergeVars = []) {

    $listId = $listId ? $listId : $this->listId;
    if (!is_string($listId)) {
      throw new \Exception('ListId must be string');
    }

    $data = array_merge([
      'email_address' => $email,
      'status' => $status,
    ], $mergeVars);

    $this->mc->post("lists/" . ($listId) . "/members", $data);

    if (!$this->mc->success()) {
      $this->logError();
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Multiple subscription in MailChimp list.
   *
   * @param array $emails
   *   List of mails.
   * @param array $listIds
   *   List of listIds.
   * @param string $status
   *   The status to apply (optional).
   */
  public function subscribeMultiple(array $emails, array $listIds, $status) {
    foreach ($emails as $email) {
      foreach ($listIds as $listId) {
        $this->subscribe($email, $listId, $status);
      }
    }
  }

  /**
   * Retourne l'objet MailCHimp.
   *
   * @return \DrewM\MailChimp\MailChimp
   *   The MailChimp object.
   */
  public function getMc() {
    return $this->mc;
  }

  /**
   * Return the ApiKey.
   *
   * @return string
   *   The api key.
   */
  public function getApiKey() {
    return $this->apiKey;
  }

  /**
   * Update user data.
   *
   * @param string $email
   *   The email.
   * @param string $listId
   *   The list Id.
   * @param array $mergedFields
   *   The mergedFields.
   * @param array $interests
   *   The interests.
   *
   * @return bool
   *   Success.
   */
  public function updateData($email, $listId = NULL, array $mergedFields = [], array $interests = []) {
    if (!empty($mergedFields) || !empty($interests)) {
      $subscriberHash = $this->getSubscriberHash($email);

      $this->mc->patch("lists/" . $listId . "/members/" . $subscriberHash, [
        'merge_fields' => $mergedFields,
        'interests' => $interests,
      ]);

      if (!$this->mc->success()) {
        $this->logError();
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Log errors.
   */
  protected function logError() {
    \Drupal::logger('MailChimp Connector')->error('Mailchimp Message : ' . $this->mc->getLastResponse()['body']);
  }

  /**
   * Retourne le subscriber Hash.
   *
   * @param string $email
   *   The email.
   *
   * @return string
   *   The hash.
   */
  public function getSubscriberHash($email) {
    return $this->mc->subscriberHash($email);
  }

}
