<?php

namespace Drupal\lmwr_tools\Shared;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Class CommandTrait.
 *
 * @package Drupal\lmwr_tools
 */
trait CommandTrait {

  /**
   * Remove UUIDs and _core from config files that must not be versioned.
   *
   * @param string $directory
   *   Export directory.
   *
   * @throws \Exception
   */
  public function removeUselessContent($directory, $removeHash = TRUE, $exportDevModules = FALSE, $removeUuid = FALSE) {
    if (!is_string($directory)) {
      throw new \Exception(sprintf('Parameter $directory must be a string, %s given', gettype($directory)));
    }

    $files = scandir($directory);

    $filePattern = '#^[^ ]+\.yml$#i';
    $exceptedFiles = ['system.site.yml'];

    $removePatterns = [];
    if ($removeUuid) {
      $removePatterns = [
        'uuidPattern' => '#^uuid: .+\n#'
      ];
    }
    if ($removeHash) {
      $removePatterns['corePattern'] = '#_core:\n {2,4}default_config_hash: .+\n#';
    }

    foreach ($files as $file) {
      // Care only about YML files.
      if (!in_array($file, $exceptedFiles) && preg_match($filePattern, $file)) {
        $fullPath = $directory . '/' . $file;
        $content = file_get_contents($fullPath);
        $content = preg_replace($removePatterns, '', $content);

        file_put_contents($fullPath, $content);
      }
    }

    // Do not export dev modules.
    if (!$exportDevModules) {
      $this->removeDevModulesFromExtensionList($directory);
    }
  }

  /**
   * Remove dev modules from core.extension.yml.
   *
   * @param string $directory
   *   The file directory.
   */
  public function removeDevModulesFromExtensionList($directory) {
    // Generate patterns.
    $removeModulePatterns = [];
    foreach ($this->getDevModules() as $moduleName) {
      $removeModulePatterns[$moduleName] = '#  ' . $moduleName . ': .+\n#';
    }

    // Rewrite core.extension.yml.
    $coreExtensionFile = $directory . '/core.extension.yml';
    if (!empty($removeModulePatterns) && file_exists($coreExtensionFile)) {
      $content = file_get_contents($coreExtensionFile);
      $content = preg_replace($removeModulePatterns, '', $content);

      file_put_contents($coreExtensionFile, $content);
    }
  }

  /**
   * Return the dev modules name list.
   *
   * @return array
   *   The list of dev modules.
   */
  public function getDevModules() {
    $composerFile = \Drupal::root() . '/../composer.json';
    if (file_exists($composerFile)) {
      $devParentModules = \GuzzleHttp\json_decode(file_get_contents($composerFile), TRUE);

      // Parse parent modules.
      $devModules = [];
      foreach (array_keys($devParentModules['require-dev']) as $packageId) {
        $packageName = substr($packageId, strpos($packageId, '/') + 1);
        if ($path = drupal_get_path('module', $packageName)) {
          $dir = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::CURRENT_AS_FILEINFO);
          $files = new RecursiveIteratorIterator($dir);

          foreach ($files as $file) {
            $name = $file->getBasename();
            if (strpos($name, '.info.yml') !== FALSE) {
              $devModules[] = str_replace('.info.yml', '', $name);
            }
          }
        }
      }

      return $devModules;
    }

    return [];
  }

}
