<?php

namespace Drupal\lmwr_tools\Command;

use Drupal\lmwr_tools\Shared\CommandTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Style\DrupalStyle;
use \Drupal\Console\Command\Config\ExportCommand as BaseCommand;

/**
 * Class ExportCommand.
 *
 * @package Drupal\lmwr_tools
 */
class ExportCommand extends BaseCommand {

  use CommandTrait;

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    parent::configure();

    $this
      ->setName('lm_config:export')
      ->setDescription($this->trans('commands.config.export.description'));
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    // Remove uuid :
    $input->setOption('remove-uuid', TRUE);

    $io = new DrupalStyle($input, $output);

    // Do export config.
    parent::execute($input, $output);

    $directory = $input->getOption('directory');

    if (!$directory) {
      $directory = config_get_config_directory(CONFIG_SYNC_DIRECTORY);
    }

    $directory = realpath(DRUPAL_ROOT . '/' . $directory);

    // Then, do post treatment.
    $this->removeUselessContent($directory);

    $io->info(
      $this->trans('commands.config.export.messages.foo')
    );
  }

}
