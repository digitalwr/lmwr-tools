<?php

namespace Drupal\lmwr_tools;

/**
 * Class LmwrToolsEvents.
 *
 * @package lmwr_tools
 */
final class LmwrToolsEvents {

  /**
   * The NODE_PUBLISHED event occurs when a node is being published.
   *
   * @Event("Drupal\lmwr_tools\Event\EntityEvent")
   */
  const NODE_PUBLISHED = "lmwr_tools.entity.node.published";

  /**
   * The NODE_UNPUBLISHED event occurs when a node is being unpublished.
   *
   * @Event("Drupal\lmwr_tools\Event\EntityEvent")
   */
  const NODE_UNPUBLISHED = "lmwr_tools.entity.node.unpublished";

  /**
   * The NODE_VIEW event occurs when a node is being seen.
   *
   * @Event("Drupal\lmwr_tools\Event\EntityEvent")
   */
  const NODE_VIEW = "lmwr_tools.entity.node.view";

  /**
   * The NODE_DELETE event occurs when a node is being deleted.
   *
   * @Event("Drupal\lmwr_tools\Event\EntityEvent")
   */
  const NODE_DELETE = "lmwr_tools.entity.node.delete";

  /**
   * The NODE_UDPATE event occurs when ...
   *
   * @Event("Drupal\lmwr_tools\Event\EntityEvent")
   */
  const NODE_UDPATE = "lmwr_tools.entity.node.udpate";

  /**
   * The NODE_CREATE event occurs when ...
   *
   * @Event("Drupal\lmwr_tools\Event\EntityEvent")
   */
  const NODE_CREATE = "lmwr_tools.entity.node.create";

  /**
   * The NODE_PRESAVE event occurs when ...
   *
   * @Event("Drupal\lmwr_tools\Event\EntityEvent")
   */
  const NODE_PRESAVE = "lmwr_tools.entity.node.presave";

  /**
   * The NODE_INSERT event occurs when ...
   *
   * @Event("Drupal\lmwr_tools\Event\EntityEvent")
   */
  const NODE_INSERT = "lmwr_tools.entity.node.insert";

  /**
   * The TAXONOMY_TERM_VIEW event occurs when ...
   *
   * @Event("Drupal\lmwr_tools\Event\EntityEvent")
   */
  const TAXONOMY_TERM_VIEW = "lmwr_tools.entity.taxonomy_term.view";

  /**
   * The TAXONOMY_TERM_DELETE event occurs when ...
   *
   * @Event("Drupal\lmwr_tools\Event\EntityEvent")
   */
  const TAXONOMY_TERM_DELETE = "lmwr_tools.entity.taxonomy_term.delete";

  /**
   * The TAXONOMY_TERM_UDPATE event occurs when ...
   *
   * @Event("Drupal\lmwr_tools\Event\EntityEvent")
   */
  const TAXONOMY_TERM_UDPATE = "lmwr_tools.entity.taxonomy_term.udpate";

  /**
   * The TAXONOMY_TERM_CREATE event occurs when ...
   *
   * @Event("Drupal\lmwr_tools\Event\EntityEvent")
   */
  const TAXONOMY_TERM_CREATE = "lmwr_tools.entity.taxonomy_term.create";

  /**
   * The TAXONOMY_TERM_PRESAVE event occurs when ...
   *
   * @Event("Drupal\lmwr_tools\Event\EntityEvent")
   */
  const TAXONOMY_TERM_PRESAVE = "lmwr_tools.entity.taxonomy_term.presave";

  /**
   * The TAXONOMY_TERM_INSERT event occurs when ...
   *
   * @Event("Drupal\lmwr_tools\Event\EntityEvent")
   */
  const TAXONOMY_TERM_INSERT = "lmwr_tools.entity.taxonomy_term.insert";

  /**
   * The ENTITY_AJAX_REQUEST event occurs when ...
   *
   * @Event("Drupal\lmwr_tools\Event\AjaxEntityEvent")
   */
  const ENTITY_AJAX_REQUEST = "lmwr_tools.entity.ajax_request";

}
