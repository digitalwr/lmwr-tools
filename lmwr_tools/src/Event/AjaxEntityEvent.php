<?php

namespace Drupal\lmwr_tools\Event;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Class EntityEvent.
 *
 * @package lmwr_tools
 */
class AjaxEntityEvent extends Event {

  /**
   * Instance of current entity.
   *
   * @var EntityInterface
   */
  private $entity;

  /**
   * Instance of reponse event.
   *
   * @var GetResponseEvent
   */
  public $responseEvent;

  /**
   * NodeViewEvent constructor.
   *
   * @param EntityInterface $entity
   *   Current entity.
   * @param GetResponseEvent $responseEvent
   *   Response event.
   */
  public function __construct(EntityInterface $entity, GetResponseEvent $responseEvent) {
    $this->entity = $entity;
    $this->responseEvent = $responseEvent;
  }

  /**
   * Get entity.
   *
   * @return EntityInterface
   *    Instance of current entity.
   */
  public function getEntity() {
    return $this->entity;
  }

}
