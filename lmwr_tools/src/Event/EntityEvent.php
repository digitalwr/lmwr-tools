<?php

namespace Drupal\lmwr_tools\Event;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class EntityEvent.
 *
 * @package lmwr_tools
 */
class EntityEvent extends Event {

  /**
   * Instance of current node.
   *
   * @var EntityInterface
   */
  private $entity;

  /**
   * Render array of the node.
   *
   * @var array
   */
  private $build;

  /**
   * Full, teaser ...etc.
   *
   * @var string
   */
  private $viewMode;

  /**
   * NodeViewEvent constructor.
   *
   * @param EntityInterface $entity
   *   Instance of current node.
   * @param array|null $build
   *   Render array of the node.
   * @param string $viewMode
   *   View mode (default "full").
   */
  public function __construct(EntityInterface $entity, array &$build = NULL, $viewMode = 'full') {
    $this->entity = $entity;
    $this->build = $build;
    $this->viewMode = $viewMode;
  }

  /**
   * Get entity.
   *
   * @return EntityInterface
   *    Instance of current entity.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Get build array.
   *
   * @return array
   *    Build array.
   */
  public function getBuild() {
    return $this->build;
  }

  /**
   * Get viewMode.
   *
   * @return string
   *   View mode (full, teaser ...etc).
   */
  public function getViewMode() {
    return $this->viewMode;
  }

}
