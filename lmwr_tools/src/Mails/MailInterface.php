<?php

namespace Drupal\lmwr_tools\Mails;

use Drupal\file\Entity\File;

/**
 * Interface MailInterface.
 *
 * @package Drupal\lmwr_tools\Mails
 */
interface MailInterface {

  /**
   * Modifie l'adresse email d'expedition.
   *
   * @param string $from
   *   L'expediteur.
   */
  public function setFrom($from);

  /**
   * Ajoute une adresse email de destination.
   *
   * @param string $to
   *   Le destinataire.
   */
  public function addTo($to);

  /**
   * Ajoute une adresse email en copie.
   *
   * @param string $cc
   *   Le destinataire de la copie.
   */
  public function addCc($cc);

  /**
   * Ajoute une adresse email de copie cachée.
   *
   * @param string $bcc
   *   Le destinataire de la copie cachée.
   */
  public function addBcc($bcc);

  /**
   * Envoie le mail.
   *
   * @return bool
   *   Le mail a été envoyé.
   */
  public function sendMail();

  /**
   * Ajoute un fichier en copie.
   *
   * @param Drupal\file\Entity\File $file
   *   Le fichier à ajouter.
   * @param string $name
   *   Le nom custom du fichier.
   */
  public function addAttachmentFile(File $file, $name = NULL);

  /**
   * Met à jour le body.
   *
   * @param string $body
   *   Le body.
   */
  public function setBody($body);

  /**
   * Met à jour le sujet de mail.
   *
   * @param string $subject
   *   Le suket.
   */
  public function setSubject($subject);

  /**
   * Met à jour l'id de l'email. Utilisé pour le template de mail.
   *
   * @param string $id
   *   L'id du mail.
   */
  public function setId($id);

  /**
   * Met à jour le reply-to.
   *
   * @param string $replyTo
   *   Le reply-to.
   */
  public function setReplyTo($replyTo);

}
