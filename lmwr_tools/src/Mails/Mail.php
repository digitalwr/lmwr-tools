<?php

namespace Drupal\lmwr_tools\Mails;

use Drupal\file\Entity\File;
use Drupal\lmwr_tools\Service\LanguageService;
use Drupal\lmwr_tools\Service\MailSender;
use Egulias\EmailValidator\Validation\RFCValidation;

/**
 * Class d'envoie de mail.
 *
 * @package Drupal\lmwr_tools\Mails
 */
class Mail implements MailInterface {

  protected $mailId;
  protected $receivers = [];
  protected $langCode;
  protected $mailData;
  protected $cc = [];
  protected $bcc = [];
  protected $checkMailValidity = TRUE;
  protected $checkMailDomain = TRUE;
  protected $theme = NULL;
  protected $defaultTheme = NULL;

  /**
   * Mail constructor.
   *
   * @param string $id
   *   L'id du mail.
   */
  public function __construct($id, $checkMailValidity = TRUE) {
    // Initialisation de l'id.
    $this->setId($id);

    // Initialisation du language.
    $this->setLangCode(LanguageService::getCurrentLanguageId());

    // Initialisation du sender.
    $siteConfig = \Drupal::config('system.site');
    $this->setFrom($siteConfig->get('mail'));

    $this->checkMailValidity = $checkMailValidity;
  }

  /**
   * Vérifie si on check le domain lors de la validation d'email.
   *
   * @return bool
   *   Le statut.
   */
  public function mustCheckMailDomain() {
    return $this->checkMailDomain;
  }

  /**
   * Modifie le statut de validation des emails.
   *
   * @param bool $checkMailDomain
   *   Le statut.
   */
  public function setCheckMailDomain($checkMailDomain) {
    $this->checkMailDomain = $checkMailDomain;
  }

  /**
   * Retourne la langue utilisée pour le mail.
   *
   * @return string
   *   La langcode.
   */
  public function getLangCode() {
    return $this->langCode;
  }

  /**
   * Modifie la langue utilisée pour le mail.
   *
   * @param string $langCode
   *   Le langCode.
   */
  public function setLangCode($langCode) {
    $this->langCode = $langCode;
  }

  /**
   * Retourne L'expéditeur.
   *
   * @return string
   *   L'expéditeur.
   */
  public function getFrom() {
    if (array_key_exists(MailSender::FIELD_FROM, $this->mailData)) {
      return $this->mailData[MailSender::FIELD_FROM];
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setFrom($from) {
    $this->mailData[MailSender::FIELD_FROM] = $from;
  }

  /**
   * Retourne L'objet du mail.
   *
   * @return string|null
   *   L'objet.
   */
  public function getSubject() {
    if (array_key_exists('subject', $this->mailData)) {
      return $this->mailData['subject'];
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setSubject($subject) {
    $this->mailData['subject'] = $subject;
  }

  /**
   * Retourne le body du mail.
   *
   * @return string
   *   Le body.
   */
  public function getBody() {
    if (array_key_exists('message', $this->mailData)) {
      return $this->mailData['message'];
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setBody($body) {
    $this->mailData['message'] = $body;
  }

  /**
   * {@inheritdoc}
   */
  public function addTo($to) {
    $this->receivers[] = $to;
  }

  /**
   * Retounrne le tableau des destinataires.
   *
   * @return array
   *   Les destinataires.
   */
  public function getTos() {
    return $this->receivers;
  }

  /**
   * {@inheritdoc}
   */
  public function addCc($cc) {
    $this->cc[] = $cc;
  }

  /**
   * Retounrne le tableau des copies.
   *
   * @return array
   *   Les copies.
   */
  public function getCcs() {
    return $this->cc;
  }

  /**
   * {@inheritdoc}
   */
  public function addBcc($bcc) {
    $this->bcc[] = $bcc;
  }

  /**
   * Retounrne le tableau des blind copies.
   *
   * @return array
   *   Les blind copies.
   */
  public function getBccs() {
    return $this->bcc;
  }

  /**
   * {@inheritdoc}
   */
  public function setId($id) {
    $this->mailId = $id;
  }

  /**
   * Retourne l'id.
   *
   * @return string
   *   L'id du mail.
   */
  public function getId() {
    return $this->mailId;
  }

  /**
   * {@inheritdoc}
   */
  public function setReplyTo($replyTo) {
    $this->mailData['Reply-to'] = $replyTo;
  }

  /**
   * {@inheritdoc}
   */
  public function getReplyTo() {
    if (array_key_exists('Reply-to', $this->mailData)) {
      return $this->mailData['Reply-to'];
    }
    return NULL;
  }

  /**
   * Modifie le theme à utiliser.
   *
   * @param string $themeName
   *   Le nom du theme à utiliser.
   */
  public function setTheme($themeName) {
    $this->theme = $themeName;
  }

  /**
   * {@inheritdoc}
   */
  public function addAttachmentFile(File $file, $name = NULL) {
    if (isset($file)) {
      return $this->addAttachment($file->getFileUri(), $name ?: $file->getFilename(), $file->getMimeType());
    }
    return FALSE;
  }

  /**
   * Ajoute un attachment.
   *
   * @param string $uri
   *   L'uri.
   * @param string|null $name
   *   Le nom du fichier.
   * @param string|null $mimeType
   *   Le mimetype.
   */
  public function addAttachment($uri, $name = NULL, $mimeType = NULL) {
    if (file_exists($uri)) {
      try {
        $uri = \Drupal::service('stream_wrapper_manager')
          ->getViaUri($uri)
          ->realpath();
        $attachment = new \stdClass();
        $attachment->uri = $uri;
        $fileData = pathinfo($uri);
        $attachment->filename = $name ?: $fileData['filename'];
        $attachment->filemime = $mimeType ?: "application/" . $fileData['extension'];
        $this->mailData['files'][] = $attachment;
        return TRUE;
      }
      catch (\Exception $e) {
        // Mute exception...
      }
    }
    return FALSE;
  }

  /**
   * Check le contenu de la données $mail.
   *
   * Si l'email n'est pas valide, on balance une erreur.
   *
   * @param string|array $mails
   *   Le mail.
   * @param string $erroType
   *   Le type d'erreur.
   * @param bool $canBeEmpty
   *   Le type de champs obligatoire ou pas.
   *
   * @return array
   *   Les emails filtrés.
   *
   * @throws \Exception
   */
  public function getFilteredMails($mails, $erroType = '', $canBeEmpty = TRUE) {
    if (is_array($mails)) {
      $mails = array_filter($mails);
      if (!$canBeEmpty && empty($mails)) {
        throw new \Exception(t('Le champs @type ne peut pas être vide', ['@type' => $erroType]));
      }
      foreach ($mails as $mail) {
        $this->checkMail($mail, $erroType, TRUE);
      }
    }
    elseif (is_string($mails)) {
      $this->checkMail($mails, $erroType, $canBeEmpty);
    }
    elseif (isset($mails) && !$canBeEmpty) {
      throw new \Exception(t('Le mail n\'est pas au bon format (@type)', ['@type' => $erroType]));
    }
    return $mails;
  }

  /**
   * Vérifie le mail.
   *
   * @param string $mail
   *   Le mail.
   */
  public function checkMail($mail, $erroType = '', $canBeEmpty = TRUE) {
    if ($this->checkMailValidity) {
      $mail = $this->getMailFromString($mail);

      // ON checke la validité du mail.
      if (\Drupal::hasService('email.validator')) {
        /** @var \Egulias\EmailValidator\EmailValidator $mailValidatorService */
        $mailValidatorService = \Drupal::service('email.validator');
        if (!$mailValidatorService->isValid($mail, null)) {
          throw new \Exception(t('Le mail @mail n\'est pas valide (@type)', [
            '@mail' => $mail,
            '@type' => $erroType
          ]));
        }
      }
    }
    elseif (!$canBeEmpty && empty($mail)) {
      throw new \Exception(t('Le mail @type ne peut pas être vide', ['@type' => $erroType]));
    }

    return TRUE;
  }

  /**
   * Retourne le mail depuis une pattern de header.
   *
   * @param string $string
   *   Le string.
   *
   * @return array|mixed
   *   Le mail.
   */
  protected function getMailFromString($string) {
    if (strpos($string, '<') !== FALSE) {
      $string = explode('<', $string);
      $string = explode('>', $string[1]);
      return reset($string);
    }
    else {
      return $string;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendMail() {
    // On vérifie la validité de l'expéditeur.
    $this->mailData[MailSender::FIELD_FROM] = $this->getFilteredMails($this->getFrom(), 'from', FALSE);

    // On vérifie si les destinataires sont présents.
    $receivers = $this->getFilteredMails($this->getTos(), 'To', FALSE);

    // On vérifie la validité des Cc.
    $cc = $this->getFilteredMails($this->getCcs(), 'Cc');
    if ($cc) {
      $this->mailData[MailSender::FIELD_HEADERS_OVERRIDE]['Cc'] = implode(',', $cc);
    }

    // On vérifie la validité des Bcc.
    $bcc = $this->getFilteredMails($this->getBccs(), 'Bcc');
    if ($bcc) {
      $this->mailData[MailSender::FIELD_HEADERS_OVERRIDE]['Bcc'] = implode(',', $bcc);
    }

    // On vérifie la validité du replyTo.
    $reply = $this->getFilteredMails($this->getReplyTo(), 'Reply-to');
    if ($reply) {
      $this->mailData[MailSender::FIELD_HEADERS_OVERRIDE]['Reply-to'] = $reply;
    }

    // Envoi du mail.
    /** @var \Drupal\Core\Mail\MailManager $mailManager */
    $mailManager = \Drupal::service('plugin.manager.mail');

    // Modification du theme pour l'envoi.
    $this->initializeTheme();

    // Envoi de l'email.
    $sent = $mailManager->mail('lmwr_tools', $this->getId(), $receivers, $this->getLangCode(), $this->mailData);

    // Récupération du thème par défaut.
    $this->reinitializeTheme();

    // Renvoi du status d'envoi.
    return $sent['result'];
  }

  /**
   * Retourne une donnée de l'email.
   *
   * @param string|null $key
   *   La clé.
   *
   * @return mixed
   *   La donnée.
   */
  public function getMessageData($key = NULL) {
    if (isset($key)) {
      if (array_key_exists($key, $this->mailData)) {
        return $this->mailData[$key];
      }
      return NULL;
    }
    return $this->mailData;
  }

  /**
   * Modifie/ajoute une donnée dans le message.
   *
   * @param string $key
   *   La clé.
   * @param mixed $data
   *   La donnée.
   */
  public function setMessageData($key, $data) {
    $this->mailData[$key] = $data;
  }

  /**
   * Modifie le theme actif à utiliser pour le mail.
   */
  protected function initializeTheme() {
    if (isset($this->theme)) {
      // Récupération du thème actif.
      /** @var \Drupal\Core\Theme\ThemeManager $themeManager */
      $themeManager = \Drupal::service('theme.manager');
      $this->defaultTheme = $themeManager->getActiveTheme();

      // Si le thème passé est différent du thème à utiliser.
      if ($this->defaultTheme->getName() !== $this->theme) {

        /** @var \Drupal\Core\Extension\ThemeHandler $themeHandler */
        $themeHandler = \Drupal::service('theme_handler');
        /** @var \Drupal\Core\Theme\ThemeInitialization $themeInitialization */
        $themeInitialization = \Drupal::service('theme.initialization');

        // Modification du theme.
        if ($themeToUse = $themeInitialization->getActiveTheme($themeHandler->getTheme($this->theme))) {
          $themeManager->setActiveTheme($themeToUse);
        }
        else {
          $this->defaultTheme = NULL;
        }
      }
      else {
        $this->defaultTheme = NULL;
      }
    }

  }

  /**
   * Remet le theme actif.
   */
  protected function reinitializeTheme() {
    if (isset($this->theme) && isset($this->defaultTheme)) {
      \Drupal::service('theme.manager')->setActiveTheme($this->defaultTheme);
    }
  }

}
