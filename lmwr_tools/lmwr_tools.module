<?php

/**
 * @file
 * Contains lmwr_tools.module..
 */

use Drupal\lmwr_tools\Service\Tracking;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\lmwr_tools\Service\Templates;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\lmwr_tools\Event\EntityEvent;
use Drupal\lmwr_tools\LmwrToolsEvents;
use Drupal\lmwr_tools\Service\EnvironmentService;
use Drupal\node\Entity\Node;

/**
 * {@inheritdoc}
 */
function lmwr_tools_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the lmwr_tools module.
    case 'help.page.lmwr_tools':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Custom tools for Drupal 8') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_entity_insert().
 *
 * Dispatch custom events to be able to create an event listener/subscriber
 * instead of an ugly hook.
 *
 * @param EntityInterface $entity
 *   Instance of the inserted entity.
 */
function lmwr_tools_entity_insert(EntityInterface $entity) {
  /** @var ContainerAwareEventDispatcher $dispatcher */
  $dispatcher = Drupal::service('event_dispatcher');
  $event = new EntityEvent($entity);

  switch ($entity->getEntityTypeId()) {
    case 'taxonomy_term':
      $eventName = LmwrToolsEvents::TAXONOMY_TERM_INSERT;
      break;

    case 'node':
      $eventName = LmwrToolsEvents::NODE_INSERT;
      break;
  }

  if (isset($eventName)) {
    $dispatcher->dispatch($eventName, $event);
  }
}

/**
 * Implements hook_entity_update().
 *
 * Dispatch custom events to be able to create an event listener/subscriber
 * instead of an ugly hook.
 *
 * @param EntityInterface $entity
 *   Instance of the inserted entity.
 */
function lmwr_tools_entity_update(EntityInterface $entity) {
  /** @var ContainerAwareEventDispatcher $dispatcher */
  $dispatcher = Drupal::service('event_dispatcher');
  $event = new EntityEvent($entity);

  switch ($entity->getEntityTypeId()) {
    case 'taxonomy_term':
      $eventName = LmwrToolsEvents::TAXONOMY_TERM_UDPATE;
      break;

    case 'node':
      $eventName = LmwrToolsEvents::NODE_UDPATE;
      break;
  }

  if (isset($eventName)) {
    $dispatcher->dispatch($eventName, $event);
  }
}

/**
 * Implements hook_entity_update().
 *
 * Dispatch custom events to be able to create an event listener/subscriber
 * instead of an ugly hook.
 *
 * @param EntityInterface $entity
 *   Instance of the inserted entity.
 */
function lmwr_tools_entity_delete(EntityInterface $entity) {
  /** @var ContainerAwareEventDispatcher $dispatcher */
  $dispatcher = Drupal::service('event_dispatcher');
  $event = new EntityEvent($entity);

  switch ($entity->getEntityTypeId()) {
    case 'taxonomy_term':
      $eventName = LmwrToolsEvents::TAXONOMY_TERM_DELETE;
      break;

    case 'node':
      $eventName = LmwrToolsEvents::NODE_DELETE;
      break;
  }

  if (isset($eventName)) {
    $dispatcher->dispatch($eventName, $event);
  }
}

/**
 * Implements hook_entity_create().
 */
function lmwr_tools_entity_create(EntityInterface $entity) {
  /** @var ContainerAwareEventDispatcher $dispatcher */
  $dispatcher = Drupal::service('event_dispatcher');
  $event = new EntityEvent($entity);

  switch ($entity->getEntityTypeId()) {
    case 'taxonomy_term':
      $eventName = LmwrToolsEvents::TAXONOMY_TERM_CREATE;
      break;

    case 'node':
      $eventName = LmwrToolsEvents::NODE_CREATE;
      break;
  }

  if (isset($eventName)) {
    $dispatcher->dispatch($eventName, $event);
  }
}

/**
 * Implements hook_entity_presave().
 */
function lmwr_tools_entity_presave(EntityInterface $entity) {
  /** @var ContainerAwareEventDispatcher $dispatcher */
  $dispatcher = Drupal::service('event_dispatcher');
  $event = new EntityEvent($entity);

  switch ($entity->getEntityTypeId()) {
    case 'taxonomy_term':
      $eventName = LmwrToolsEvents::TAXONOMY_TERM_PRESAVE;
      break;

    case 'node':
      $eventName = LmwrToolsEvents::NODE_PRESAVE;
      break;
  }

  if (isset($eventName)) {
    $dispatcher->dispatch($eventName, $event);
  }

  // Dispatch event when a Node entity's status changed.
  if ($entity instanceof Node && isset($entity->original)) {
    // Status of the node before being saved.
    $previousStatus = $entity->original->status->value;

    // Status of the node after being saved.
    $newStatus = $entity->status->value;

    if ($previousStatus == NODE_NOT_PUBLISHED && $newStatus == NODE_PUBLISHED) {
      // Dispatch node published event.
      $dispatcher->dispatch(LmwrToolsEvents::NODE_PUBLISHED, $event);
    }
    elseif ($previousStatus == NODE_PUBLISHED && $newStatus == NODE_NOT_PUBLISHED) {
      // Dispatch node unpublished event.
      $dispatcher->dispatch(LmwrToolsEvents::NODE_UNPUBLISHED, $event);
    }
  }
}

/**
 * Implements hook_entity_view().
 *
 * Dispatch custom events to be able to create an event listener/subscriber
 * instead of an ugly hook.
 *
 * @param array $build
 *   A renderable array representing the entity content.
 * @param EntityInterface $entity
 *   The entity object.
 * @param EntityViewDisplayInterface $display
 *   The entity view display holding the display options configured for the
 *   entity components.
 * @param string $view_mode
 *   The view mode the entity is rendered in.
 */
function lmwr_tools_entity_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  /** @var ContainerAwareEventDispatcher $dispatcher */
  $dispatcher = Drupal::service('event_dispatcher');
  $event = new EntityEvent($entity, $build, $view_mode);

  switch ($entity->getEntityTypeId()) {
    case 'taxonomy_term':
      $eventName = LmwrToolsEvents::TAXONOMY_TERM_VIEW;
      break;

    case 'node':
      $eventName = LmwrToolsEvents::NODE_VIEW;
      break;
  }

  if (isset($eventName)) {
    $dispatcher->dispatch($eventName, $event);
  }
}

/**
 * {@inheritdoc}
 */
function lmwr_tools_requirements($phase) {
  if ($phase == 'runtime') {
    $env = \Drupal::service(EnvironmentService::SERVICE_NAME)->getEnvironment();
    $requirements['environment_var'] = [
      'title'    => t('Environment variable'),
      'value'    => $env ? $env : t('Undefined environment constant'),
      'severity' => $env ? REQUIREMENT_INFO : REQUIREMENT_ERROR,
    ];
  }
  return $requirements;
}

/**
 * {@inheritdoc}
 */
function lmwr_tools_toolbar() {
  $items = [];
  /** @var \Drupal\Core\Menu\LocalTaskManagerInterface $manager */
  $manager = \Drupal::service('plugin.manager.menu.local_task');
  $localTasks = $manager->getLocalTasks(\Drupal::routeMatch()->getRouteName());
  uasort($localTasks['tabs'], function ($a, $b) {
    return $a['#weight'] > $b['#weight'];
  });

  // Build a list of links for the menu.
  $links = [];

  if (!empty($localTasks['tabs'])) {
    foreach ($localTasks['tabs'] as $routeName => $localTaskItem) {
      $links[$routeName] = [
        '#type'   => 'link',
        '#title'  => $localTaskItem['#link']['title'],
        '#url'    => $localTaskItem['#link']['url'],
        '#access' => $localTaskItem['#access'],
      ];
    }
  }

  $items['tabs'] = [
    '#cache'  => ['max-age' => 0],
    '#type'   => 'toolbar_item',
    'tab'     => [
      '#type'  => 'link',
      '#title' => t('Actions'),
      '#url'   => Url::fromRoute('<front>'),
    ],
    'tray'    => [
      'content' => [
        '#theme'      => 'item_list',
        '#type'       => 'ul',
        '#items'      => $links,
        '#attributes' => [
          'class' => ['toolbar-menu'],
        ],
      ],
    ],
    '#weight' => 99,
  ];
  return $items;
}

/**
 * Implements hook_mail().
 */
function lmwr_tools_mail($key, &$message, $params) {
  \Drupal\lmwr_tools\Service\MailSender::me()
    ->prepareMail($key, $message, $params);
}

/**
 * Add system error suggestion pages.
 *
 * @param array $variables
 *   .
 *   Data.
 *
 * @return array
 *   Suggestions.
 */
function lmwr_tools_theme_suggestions_page(array $variables) {
  $path_args = explode('/', trim(\Drupal::service('path.current')
    ->getPath(), '/'));
  $suggestions = theme_get_suggestions($path_args, 'page');
  $http_error_suggestions = [
    'system.401' => 'page__401',
    'system.403' => 'page__403',
    'system.404' => 'page__404',
  ];
  $route_name = \Drupal::routeMatch()->getRouteName();
  if (isset($http_error_suggestions[$route_name])) {
    $suggestions[] = $http_error_suggestions[$route_name];
  }
  return array_unique($suggestions);
}

/**
 * Create a details tab on node form.
 *
 * {@inheritdoc}
 */
function lmwr_tools_form_node_form_alter(&$form, $form_state) {
  Templates::me()->hookFormNodeFormAlter($form, $form_state);
}

/**
 * Add node theme suggestion according page template widget.
 *
 * {@inheritdoc}
 */
function lmwr_tools_theme_suggestions_node_alter(array &$suggestions, array $variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $variables['elements']['#node'];
  foreach ($node->getFieldDefinitions() as $key => $fieldDefinition) {
    if ($fieldDefinition->getType() === 'lmwr_theme_template_field_type') {
      $suggestions[] = 'node__' . $node->{$key}->value;
      $suggestions[] = 'node__' . $node->{$key}->value . '__' . $variables['elements']['#view_mode'];
    }
  }
}

/**
 * {@inheritdoc}
 */
function lmwr_tools_entity_operation_alter(array &$operations, EntityInterface $entity) {
  if ($entity instanceof ConfigEntityBase) {
    if (!array_key_exists('duplicate', $operations)) {
      $operations['duplicate'] = [
        "title"  => t('Duplicate'),
        "weight" => 35,
        "url"    => Url::fromRoute('lmwr_tools.duplicate_config_entity_type', [
          'type'           => $entity->getEntityTypeId(),
          'initial_bundle' => $entity->id(),
          'redirect'       => \Drupal::routeMatch()->getRouteName(),
        ]),
      ];
    }
  }
}

/**
 * Implements hook_cron().
 * {@inheritdoc}.
 */
function lmwr_tools_cron() {
  if (!empty(Tracking::me()->getCurrentProjectId()) && EnvironmentService::me()
      ->isProduction()) {
    Tracking::me()->sendData();
  }

}
