<?php

use \Drupal\lmwr_tools\Drush\LmwrToolsCommandsDrush8;

function lmwr_tools_drush_command() {

  /**
   * ATTENTION !!!
   *
   * Les commandes drush doivent être disponibles pour les version 8 et 9, pour gérer
   * une retrocompat.
   * Donc ce fichier gère uniquement l'interface pour drush 8.
   * Il ne doit pas y avoir de contenu ici, car il ne sera pas lu par Drush 9.
   *
   * Il faut créer vos fonctions au format Drush 9 (avec déclarations des variables via dockblocks)
   * dans le fichier \LmwrToolsCommandsTrait.
   * Ensuite vous déclarez vos commandes via l'interface Drush 8 ci-dessous.
   * Et vous faite appelles à la classe LmwrToolsCommandsDrush8 qui utilise le trait précédemment cité.
   */
  $commandList = [
    'set_module_version'   => [
      'description' => 'Set hook',
      'aliases'     => ['smv'],
      'arguments'   => [
        'moduleName' => 'Module name',
        'version'    => 'Update Id',
      ],
    ],
    'lm-config-export'     => [
      'description' => 'Export without uuid',
      'aliases'     => ['lmcex'],
      'options'     => [
        'destination'    => 'Destination repository',
        'remove-hash'    => 'Remove config hash',
        'without-export' => 'Remove uuid whithout export  configuration',
        'export-dev'     => 'Export dev',
        'remove-uuid'    => 'Remove uuid',
      ]
    ],
    'generate-po-file'     => [
      'aliases'   => ['gpo'],
      'arguments' => [
        'type' => 'Type (module, theme)',
        'name' => 'Name',
      ],
    ],
    'reload-module-config' => [
      'aliases'   => ['rmc'],
      'arguments' => [
        'module' => 'Module name',
      ],
    ]
  ];

  return $commandList;
}

/**
 * Reload module default config
 *
 * @param $module
 */
function drush_lmwr_tools_reload_module_config($module) {
  /** @var \Drupal\lmwr_tools\Drush\LmwrToolsCommandsDrush8 $lmwrToolsDrushCommands8 */
  $lmwrToolsDrushCommands8 = \Drupal::service(LmwrToolsCommandsDrush8::SERVICE_NAME);
  $lmwrToolsDrushCommands8->reloadModuleConfig($module);
}

/**
 * Generate PO file.
 *
 * @param null $type
 * @param null $name
 */
function drush_lmwr_tools_generate_po_file($type = NULL, $name = NULL) {
  /** @var \Drupal\lmwr_tools\Drush\LmwrToolsCommandsDrush8 $lmwrToolsDrushCommands8 */
  $lmwrToolsDrushCommands8 = \Drupal::service(LmwrToolsCommandsDrush8::SERVICE_NAME);
  $lmwrToolsDrushCommands8->generatePoFile($type, $name);
}

/**
 * Set the module version in order to redo hook_updates
 * @param null $moduleName
 * @param null $version
 */
function drush_lmwr_tools_set_module_version($moduleName = NULL, $version = NULL) {
  /** @var \Drupal\lmwr_tools\Drush\LmwrToolsCommandsDrush8 $lmwrToolsDrushCommands8 */
  $lmwrToolsDrushCommands8 = \Drupal::service(LmwrToolsCommandsDrush8::SERVICE_NAME);
  $lmwrToolsDrushCommands8->setModuleVersion($moduleName, $version);
}


/**
 * Export conf and remove uuid.
 */
function drush_lmwr_tools_lm_config_export() {
  $options = [
    'destination'    => drush_get_option('destination'),
    'without-export' => drush_get_option('without-export'),
    'remove-hash'    => drush_get_option('remove-hash'),
    'remove-uuid'    => drush_get_option('remove-uuid'),
    'export-dev'     => drush_get_option('export-dev'),
  ];

  /** @var \Drupal\lmwr_tools\Drush\LmwrToolsCommandsDrush8 $lmwrToolsDrushCommands8 */
  $lmwrToolsDrushCommands8 = \Drupal::service(LmwrToolsCommandsDrush8::SERVICE_NAME);
  $lmwrToolsDrushCommands8->lmConfigExport($options);
}