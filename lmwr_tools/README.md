# LMWR tools
A Drupal 8 module shipping redundant functionalities.

## use drupal mail service
```
$langcode = \Drupal::currentUser()->getPreferredLangcode();
\Drupal::service('plugin.manager.mail')->mail('lmwr_tools', 'foo_example', $to, $langcode, [
    'subject' => 'Subject',
    'message' => 'Message',
  ]);
```

To use a twig template, create file with a similar name › *swiftmailer--lmwr-tools--foo-example.hml.twig* in your theme templates folder (dummy_theme/templates/mails)

```
#base-mail.html.twig
{% if message.recipients %}
    {{ message.recipients }}
    <hr>
{% endif %}

{% block body %}{% endblock %}


{% block footer %}
    <br>
    <br>
    Footer
{% endblock %}
```

```
#swiftmailer--lmwr-tools--foo-example.hml.twig
{% extends "@dummy_theme/mails/base-mail.html.twig" %}

{% block body %}
    Foo example
    {{ message.body }}
{% endblock %}

```

## use mail chimp connector

This module brings a MailChimp connexion that requires 2 parameters to be set.
With the use of the MailChimpConnector class. You have to instance an object by apikey.

Usage : 
```
$mailChimpConnector = new MailChimpConnector( '{your_api_key}' );

// define a listId : 
$mailChimpConnector->setListId( '{your_list_id}' );

// subscribe : 
$mailChimpConnector->subscribe('foof@foo.fr');

// Force subscription to a specifiv list : 
$mailChimpConnector->subscribe('foof@foo.fr' , 'your_specific_list_id');

// Multiple subscription
$mailChimpConnector->subscribeMultiple(['foof@foo.fr','foo2@foo.fr']);

// Multiple subscription to multiple lists : 
$mailChimpConnector->subscribeMultiple(['foof@foo.fr','foo2@foo.fr'] , ['list_id_1','list_id2']);

// Subscribe one user to several lists 
$mailChimpConnector->subscribe(['foof@foo.fr'] , ['list_id_1','list_id2']);
```
## Page templates
A template page widget is available in the fields management of a node.

This allows you to choose page templates defined in the default theme.
You should think about adding your page templates in the info.yml file of your current theme, as follows:
```
pages-templates:
  machine_name: 'Label of template'
```

## Drush commands :
### drush set_module_version (alias smv) :
Set the module version in order to redo hook_updates.

Parameters : 
 - module_name : The module to downgrade
 - version : the version in which you want to downgrade.
 
Examples :
    ```drush smv lmwr_tools 8000```
    

### drush lm-config-export (alias lmcex) :
Export conf and remove uuid.

options : 
- --destination : Destination directory. 
- --remove-hash : Remove the hash if set to 1
- --without-export : Do not export conf if set to 1

Examples :
   ```drush lmcex --directory=config/export --remove-hash=1 --without-export=1```
   
## Multilingue menus
Hide untranslated menu items on front.
```
/**
* {@inheritdoc}
*/
function MY_MODULE_preprocess_menu(&$variables) {
 if ($variables['menu_name'] == 'main') {
   LanguageService::filterMenusByCurrentLanguage($variables['items']);
 }
}
```

Hide untranslated menu items on admin form (/admin/structure/menu/manage/main)
```
/**
* {@inheritdoc}
*/
function MYMODULE_form_menu_edit_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
 LanguageService::filterFormMenusByCurrentLanguage($form);
}
```

## Staging mail recipient
You can define the staging email recipient in your `settings.local.php`
```
$config['lmwr.settings']['staging_mail_recipient'] = 'myemail';
```

## Translations POT file generator
You can generate automatically module/theme POT file with drush.
```
drush gpo module my_module
drush gpo theme my_theme
```
> Note: A pot file will be automatically generated in your module/theme translations subfolder.