<?php

namespace Drupal\lmwr_redirect\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\redirect\Entity\Redirect;

/**
 * Class ImportRedirect.
 *
 * @package Drupal\lmwr_redirect\Form
 */
class ImportRedirect extends FormBase {

  const FORM_ID = 'lmwr_redirect.redirect_import';
  const FIELD_FILE = 'file';
  const FIELD_STATUS = 'status';
  const SEPARATOR = ';';
  const WRAPPER = '"';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return static::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $authorised = 'csv';
    $form[static::FIELD_FILE] = [
      '#type'              => 'managed_file',
      '#title'             => t('Fichier'),
      '#upload_validators' => [
        'file_validate_extensions' => [$authorised],
        'file_validate_size'       => array(25600000)
      ],
      '#description'       => 'Types authorisés : ' . $authorised
      . '<br/>Separateur : "' . static::SEPARATOR . '" <br/>Wrapper : "' . static::WRAPPER . '" <br/><br/>Ex:<br/>"/mon-ancienne-url";"/fr/ma-nouvelle-url"',
    ];

    $form[static::FIELD_STATUS] = [
      '#type'          => 'select',
      '#title'         => t('Redirect status'),
      '#description'   => t('You can find more information about HTTP redirect status codes at <a href="@status-codes">@status-codes</a>.', array('@status-codes' => 'http://en.wikipedia.org/wiki/List_of_HTTP_status_codes#3xx_Redirection')),
      '#default_value' => $form_state->getValue('status') ?: '301',
      '#options'       => redirect_status_code_options(),
    ];

    $form['save'] = [
      '#type'        => 'submit',
      '#value'       => t('Enregistrer'),
      '#button_type' => 'primary'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($redirections = $this->getRedirectionList($form_state)) {
      // Supression des anciennes redirections.
      $this->deleteOldRedirections(array_keys($redirections));

      // Ajout des nouvelles redirections.
      $this->addNewRedirections($redirections, $form_state->getValue(static::FIELD_STATUS));
    }
  }

  /**
   * Retourne la liste des éléments dans le csv.
   *
   * @param FormStateInterface $formState
   *   Le formState.
   *
   * @return array|null
   *   The redirection list.
   */
  protected function getRedirectionList(FormStateInterface $formState) {

    // On récupère le fichier.
    if ($file = File::load($formState->getValue(static::FIELD_FILE)[0])) {
      // On parse le fichier.
      $path = $file->getFileUri();

      if ($path) {
        if ($handle = fopen($path, 'r')) {
          $redirections = [];

          while (FALSE !== ($data = fgetcsv($handle, NULL, static::SEPARATOR, static::WRAPPER))) {
            $redirectUrl = trim($data[1]);
            $source = trim($data[0]);
            while ($source[0] == '/') {
              $source = substr($source, 1);
            }

            if (!empty($source) && !empty($redirectUrl)) {
              $redirections[$source] = utf8_decode($redirectUrl);
            }
          }

          if (empty($redirections)) {
            return NULL;
          }
          return $redirections;
        }
      }
    }
    return NULL;
  }

  /**
   * Supprime les anciennes sources.
   *
   * @param array $oldSources
   *   La vieille source.
   */
  protected function deleteOldRedirections(array $oldSources) {
    $query = \Drupal::entityQuery('redirect');
    $query->condition('redirect_source.path', $oldSources, 'IN');
    $redirectIds = $query->execute();
    /** @var Redirect[] $oldRedirects */
    $oldRedirects = Redirect::loadMultiple($redirectIds);
    foreach ($oldRedirects as $redirect) {
      $redirect->delete();
    }
  }

  /**
   * Ajoute les nouveux éléments.
   *
   * @param array $redirections
   *   La redirection.
   * @param int $type
   *   Le type de redirection.
   */
  protected function addNewRedirections(array $redirections, $type = 301) {
    foreach ($redirections as $source => $redirectUrl) {
      $redirect = Redirect::create([]);
      $redirect->setSource(trim($source));
      $redirect->setRedirect($redirectUrl);
      $redirect->setStatusCode($type);
      $redirect->save();
    }

    \drupal_set_message(t('@count redirections créées', ['@count' => count($redirections)]));
  }

}
