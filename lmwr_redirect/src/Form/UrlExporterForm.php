<?php

namespace Drupal\lmwr_redirect\Form;

use Cocur\Slugify\Slugify;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\lmwr_tools\Service\LanguageService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UrlExporterForm.
 *
 * @package Drupal\lmwr_redirect\Form
 */
class UrlExporterForm extends FormBase {

  const URL_RELATIVE = 'rel';
  const URL_ABSOLUTE = 'abs';
  const TYPE = 'type';
  const TMP_DIR = 'temporary://';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lmwr_redirect.url_exporter';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Donwoload.
    $this->downloadFile($form);

    // Paramètres.
    $form[static::TYPE] = [
      '#type'    => 'select',
      '#title'   => t('Type d\'url'),
      '#options' => [
        static::URL_RELATIVE => 'relatives',
        static::URL_ABSOLUTE => 'absolues',
      ]
    ];
    $form['submit'] = [
      '#type'        => 'submit',
      '#value'       => t('Exporter les urls'),
      '#button_type' => 'primary'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Récupération des urls.
    $urls = $this->getUrls();

    // Génération du fichier .
    if ($fileName = $this->generateCsv($urls, $form_state->getValue(static::TYPE))) {
      // Redirection.
      $routeMatch = \Drupal::routeMatch();
      $url = Url::fromRoute($routeMatch->getRouteName(), [], ['query' => ['file' => $fileName]]);
      $form_state->setRedirectUrl($url);
    }

  }

  /**
   * Renvoie la liste des alias d'urls dans la langue courante.
   *
   * @return array
   *   Les urls.
   */
  protected function getUrls() {
    $query = \Drupal::database()->select('url_alias', 'u');
    $query->condition('langcode', LanguageService::getCurrentLanguageId());
    $query->groupBy('alias');
    $query->fields('u', ['alias']);
    return $query->execute()->fetchCol();
  }

  /**
   * Génère le fichier temporaire.
   *
   * @param array $urls
   *   La liste des urls à exporter.
   * @param string $type
   *   Le type d'url.
   *
   * @return string|null
   *   Le nom du fichier généré.
   */
  protected function generateCsv(array $urls, $type) {
    // Génération du fichier.
    $fileName = $this->getFilePrefix() . '_url_' . LanguageService::getCurrentLanguageId() . '_' . date('d-m-Y-H-i-s') . '.csv';
    if ($file = fopen(static::TMP_DIR . $fileName, 'a+')) {
      $prefix = $type === static::URL_ABSOLUTE ? \Drupal::request()
        ->getSchemeAndHttpHost() : '';
      foreach ($urls as $url) {
        fputcsv($file, [$prefix . $url]);
      }
      fclose($file);

      return $fileName;
    }
    else {
      \Drupal::messenger()
        ->addError('Une erreur d\'écriture du fichier temporaire s\'est produite');
    }
  }

  /**
   * Ajoute une iframe de téléchargement.
   *
   * @param array $form
   *   Le formulaire.
   */
  protected function downloadFile(array &$form) {
    if (\Drupal::request()->query->has('file')) {
      $filePath = static::TMP_DIR . \Drupal::request()->query->get('file');
      if (file_exists($filePath)) {
        $form['download'] = [
          '#markup'       => '<iframe class="visually-hidden" src="' . Url::fromRoute('lmwr_redirect.url_exporter.download', ['file' => \Drupal::request()->query->get('file')])
            ->toString() . '"></iframe>',
          '#allowed_tags' => ['iframe'],
        ];
      }
    }
  }

  /**
   * Fonction appelé lors du force download.
   */
  public static function forceDownload() {
    $fileName = \Drupal::routeMatch()->getParameter('file');
    $filePath = static::TMP_DIR . $fileName;
    if ($filePath) {
      $response = new Response(file_get_contents($filePath));

      $disposition = $response->headers->makeDisposition(
        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
        basename($filePath)
      );
      $response->headers->set('Content-Disposition', $disposition);

      // Suppression du fichier.
      unlink($filePath);

      return $response;
    }

    throw new NotFoundHttpException();
  }

  /**
   * Retourne le prefix du fichier.
   */
  protected function getFilePrefix() {
    $siteConfig = \Drupal::config('system.site');
    return Slugify::create([])->slugify($siteConfig->get('name'));
  }

}
