# LMWR FORM
Use this module in order to ad custom form in renderable entity

## How to ?
 1. Enable this module
 2. Create a custom Lmwr Form Plugin in /your_module/src/Plugin/lmwr/lmwr_form
 3. Clear the cash (drush cr) in order to reload plugins
 4. Add a Custom Form Field in your entity structure (Node, Term, or whatever)
 5. Choose your custom form in the selector
 6. That's all. (Normally) You should see the form in your entity in front.
 
## How to implement the Plugin 
 1. Create a custom class in /your_module/src/Plugin/lmwr/lmwr_form/. 
  > Ex: /your_module/src/Plugin/lmwr/lmwr_form/MyCustomForm.php
 2. This class must extends LmwrFormBase (itseld extending FormBase)
 ```
 class ContactForm extends LmwrFormBase
 ``` 
 3. Add the form id as the plugin id, and a readable name for the selector field in the annotation
 
 ```
  /**
   * Plugin implementation of the lmwr_form
   *
   * @LmwrFormAnnotation(
   *   id = "my_custom_form",
   *   name = "Mon formulaire custom",
   * )
   */
  ```
 4. Finally you should have something like this : 
 ```
<?php
namespace Drupal\module\Plugin\lmwr\lmwr_form;


use Drupal\Core\Form\FormStateInterface;
use Drupal\lmwr_form\Base\LmwrFormBase;



/**
 * Plugin implementation of the lmwr_form
 *
 * @LmwrFormAnnotation(
 *   id = "my_custom_form",
 *   name = "Mon formulaire custom",
 * )
 */
 class MyCustomForm extends LmwrFormBase
 {
   const FORM_ID = 'my_custom_form';
 
   public function getFormId()
   {
     return self::FORM_ID;
   }
 }
   ```
   
 5. Then you can override FormBase methods as well (buildForm , submitForm, etc...) 

## Tricks
 1. You can define the access of the lmwr_form_field in BO in the permission grid with the 'administer_lmwr_form' permission
 2. You can define different buildForm and submitForm depending on the view mode of the parent entity : 
  ```
  public function buildFormFull(array $form, FormStateInterface $form_sate){
    return $form;
  }
  public function buildFormFull(array $form, FormStateInterface $form_sate){
    return $form;
  }
  ```
  3. You can access the field vars using ``` $this->getLmwrFormFieldVars( $form_state ); ```
  4. You can access the parent field object (Node or Taxonomy Term) using ``` $this->getFieldParentObject( $form_state ); ```
  5. You can access the parent object view_mode using ```$this->getParentObjectViewMode( $form_state );```
  
  