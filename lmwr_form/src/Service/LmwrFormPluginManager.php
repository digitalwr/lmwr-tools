<?php

namespace Drupal\lmwr_form\Service;

use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class LmwrFormPluginManager.
 *
 * @package lmwr_form
 */
class LmwrFormPluginManager extends DefaultPluginManager {

  /**
   * Service Id.
   */
  const SERVICE_NAME = 'plugin.manager.lmwr_form.lmwr_form_plugin_manager';

  /**
   * Plugin package.
   */
  const PACKAGE_NAME = 'lmwr/lmwr_form';

  /**
   * LmwrFormPluginManager constructor.
   *
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/' . self::PACKAGE_NAME, $namespaces, $module_handler, 'Drupal\lmwr_form\Base\LmwrFormBaseInterface', 'Drupal\lmwr_form\Annotation\LmwrFormAnnotation');

    $this->alterInfo('lmwr_form_annotation_info');
    $this->setCacheBackend($cache_backend, 'lmwr_form_annotation_info');
  }

  /**
   * Return the LmwrFormBase by form_id :.
   *
   * @param string $form_id
   *   The ID of the form.
   * @param FormStateInterface $form_state
   *   The form object.
   *
   * @return mixed|null|object
   *   The form build or NULL.
   */
  public function getInstanceByFormId($form_id, FormStateInterface $form_state) {

    $definitions = $this->getDefinitions();
    if (array_key_exists($form_id, $definitions)) {
      return \Drupal::formBuilder()->buildForm($definitions[$form_id]['class'], $form_state);
    }
    return NULL;
  }

}
