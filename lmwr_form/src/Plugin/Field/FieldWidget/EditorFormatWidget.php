<?php

namespace Drupal\lmwr_form\Plugin\Field\FieldWidget;

use Drupal\editor\Entity\Editor;
use Drupal\text\Plugin\Field\FieldWidget\TextareaWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'lmwr_form_text_editor_format' widget.
 *
 * @FieldWidget(
 *   id = "lmwr_form_text_editor_format",
 *   label = @Translation("Format d'éditeur"),
 *   field_types = {
 *     "text_long"
 *   },
 * )
 */
class EditorFormatWidget extends TextareaWidget {


  /**
   * The list of editor formats.
   *
   * @var null
   */
  static protected $editorsList = NULL;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'editor_type' => static::getDefaultEditor(),
      'force_type'  => TRUE
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element['editor_type'] = [
      '#type'          => 'select',
      '#options'       => self::getAvailableEditors(),
      '#default_value' => $this->getSetting('editor_type'),
    ];
    $element['force_type'] = [
      '#type'          => 'checkbox',
      '#title'         => t('Ne pas permettre le choix d\'autres types d\'éditeur'),
      '#default_value' => $this->getSetting('force_type'),
    ];
    unset($element['rows']);
    return $element;
  }

  /**
   * Return the list of available editors.
   *
   * @return array|null
   *   The list of available editors name.
   */
  static protected function getAvailableEditors() {
    if (!isset(self::$editorsList)) {
      self::$editorsList = array_map(function (Editor $editor) {
        return $editor->label();
      }, Editor::loadMultiple());
    }
    return self::$editorsList;
  }

  /**
   * Return the default Editor (FULL HTML).
   *
   * @return string
   *   The ID of the defaukt editor format.
   */
  static protected function getDefaultEditor() {
    $data = array_keys(self::getAvailableEditors());
    return reset($data);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t('Type: @editor_type', ['@editor_type' => self::getAvailableEditors()[$this->getSetting('editor_type')]]);
    if ($this->getSetting('force_type')) {
      $summary[] = t('Ne permet pas la selection d\'autres types d\'éditeur');
    }
    else {
      $summary[] = t('Autorise la selection d\'autres types d\'éditeur');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['#format'] = $this->getSetting('editor_type');

    if ($this->getSetting('force_type')) {
      $element['#attributes']['class'][] = 'lmwr_form_force_editor_type';
      $form['#attached']['library'][] = 'lmwr_form/editor_format_widget';
    }

    return $element;
  }

}
