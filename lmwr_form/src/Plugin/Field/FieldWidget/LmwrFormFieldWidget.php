<?php

namespace Drupal\lmwr_form\Plugin\Field\FieldWidget;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\lmwr_form\Service\LmwrFormPluginManager;

/**
 * Plugin implementation of the 'field_example_text' widget.
 *
 * @FieldWidget(
 *   id = "lmwr_form_field_widget",
 *   module = "lmwr_form",
 *   label = @Translation("Test"),
 *   field_types = {
 *     "lmwr_form_field"
 *   }
 * )
 */
class LmwrFormFieldWidget extends WidgetBase {

  /**
   * Instance of the service.
   *
   * @var LmwrFormPluginManager
   */
  protected $lmwrFormManager;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $element += array(
      '#type' => 'select',
      '#default_value' => $value,
      '#options' => $this->getLmwrFormSelectList(),
      '#element_validate' => array(
        array($this, 'validate'),
      ),
      '#access' => \Drupal::currentUser()->hasPermission('administer_lmwr_form')
    );
    return array('value' => $element);
  }

  /**
   * Validate the color text field.
   */
  public function validate($element, FormStateInterface $form_state) {

    if (!empty($element['#value']) && !array_key_exists($element['#value'], $this->getLmwrFormManager()->getDefinitions())) {
      $form_state->setError($element, t("The form is not a valid lmwr form. Please check your annotation."));
    }
  }

  /**
   * TODO description.
   */
  protected function getLmwrFormSelectList() {
    /** @var LmwrFormPluginManager $lmwr_form_manager */
    $lmwr_form_manager = \Drupal::service(LmwrFormPluginManager::SERVICE_NAME);
    $options = ['' => t('Choisisser le formulaire à afficher')];
    foreach ($lmwr_form_manager->getDefinitions() as $form_id => $data) {
      $options[$form_id] = $data['name'];
    }
    return $options;
  }

  /**
   * Return LmwrFormManagerPlugin.
   *
   * @return LmwrFormPluginManager
   *   Instance of the service.
   */
  protected function getLmwrFormManager() {
    if (!$this->lmwrFormManager) {
      $this->lmwrFormManager = \Drupal::service(LmwrFormPluginManager::SERVICE_NAME);
    }
    return $this->lmwrFormManager;
  }

}
