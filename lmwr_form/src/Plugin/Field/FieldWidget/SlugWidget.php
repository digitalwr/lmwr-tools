<?php

namespace Drupal\lmwr_form\Plugin\Field\FieldWidget;

use Cocur\Slugify\Slugify;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the 'slug_widget' widget.
 *
 * @FieldWidget(
 *   id = "slug_widget",
 *   label = @Translation("Slug widget"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class SlugWidget extends WidgetBase {
  use StringTranslationTrait;

  /**
   * Separator.
   *
   * @const string
   */
  const FIELD_SEPARATOR = 'separators';

  /**
   * Slugifier.
   *
   * @var \Cocur\Slugify\Slugify
   */
  protected $slugifier;

  /**
   * SlugWidget constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->slugifier = Slugify::create(['separator' => $this->getSetting(static::FIELD_SEPARATOR)]);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        static::FIELD_SEPARATOR => '_'
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    // Récupération de tous les types d'entités.
    $elements[static::FIELD_SEPARATOR] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Separateur'),
      '#default_value' => $this->getSetting(static::FIELD_SEPARATOR),
      '#required'      => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Separator: @separator', ['@separator' => $this->getSetting(static::FIELD_SEPARATOR)]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type'          => 'textfield',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
    ];

    $parent = $items->getParent();
    if ($parent
      && method_exists($parent, 'getEntity')
      && $parent->getEntity()->id()) {
      if (\Drupal::currentUser()->hasPermission('slug_widget_modification')) {
        $element['value']['#description'] = $this->t('Attention ! En modifiant cet identifiant vous risquez d\'affecter certaines fonctionnalités');
      }
      else {
        $element['value']['#disabled'] = TRUE;
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $values = parent::massageFormValues($values, $form, $form_state);

    foreach ($values as &$value) {
      $value['value'] = $this->slugifier->slugify($value['value']);
    }

    return $values;
  }

}
