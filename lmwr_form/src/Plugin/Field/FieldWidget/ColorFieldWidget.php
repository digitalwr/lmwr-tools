<?php

namespace Drupal\lmwr_form\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'lmwr_color_field' widget.
 *
 * @FieldWidget(
 *   id = "lmwr_color_field_widget",
 *   label = @Translation("Color widget"),
 *   field_types = {
 *     "lmwr_color_field"
 *   },
 * )
 */
class ColorFieldWidget extends WidgetBase {

  const SEPARATOR = ',';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'type' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'lmwr_form/color_field_widget';

    $element += [
      '#type'          => 'textfield',
      '#default_value' => $items[$delta]->value,
      '#attributes'    => [
        'class' => ['color_picker', 'visually-hidden']
      ],
    ];

    $colorsList = $this->getColorsArray($this->getSetting('type'));
    if (!empty($colorsList)) {
      $element['#attributes']['data-palette'] = implode(',', $colorsList);
    }
    return array('value' => $element);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return parent::settingsForm($form, $form_state);
    // @todo : Palette only not working ....
    $element = parent::settingsForm($form, $form_state);
    $element['type'] = [
      '#type'          => 'textarea',
      '#title'         => t('Couleurs autorisées'),
      '#default_value' => implode(self::SEPARATOR, $this->getColorsArray($this->getSetting('type'))),
      '#description'   => t('Séparez les couleurs par des virgules (\'@separator\')', ['@separator' => self::SEPARATOR])
    ];
    return $element;
  }

  /**
   * Return the sanitized authorised color list.
   *
   * @param string $values
   *   The stored color list.
   *
   * @return array
   *   The list of colors.
   */
  protected function getColorsArray($values) {
    $values = explode(self::SEPARATOR, $values);
    $values = array_map([$this, 'sanitizeColor'], $values);
    $values = array_filter($values);
    return $values;
  }

  /**
   * Sanitize the color.
   *
   * @param string $color
   *   The input color string.
   *
   * @return null|string
   *   The sanitized color.
   */
  protected function sanitizeColor($color) {
    $color = trim($color);
    if (!empty($color) && $color[0] == '#' && strlen($color) == 7) {
      if (ctype_xdigit(substr($color, 1))) {
        return $color;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    // @todo : Palette only not working ....
    return [];

    $summary = [];
    $summary[] = t('Type: @type', ['@type' => empty($this->getColorsArray($this->getSettings()['type'])) ? t('full') : t('palette')]);

    return $summary;
  }

}
