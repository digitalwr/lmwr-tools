<?php

namespace Drupal\lmwr_form\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'french_zip_code_widget' widget.
 *
 * @FieldWidget(
 *   id = "french_zip_code_widget",
 *   label = @Translation("French zip code widget"),
 *   field_types = {
 *     "french_zip_code_type"
 *   }
 * )
 */
class FrenchZipCodeWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size'        => 60,
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['size'] = [
      '#type'          => 'number',
      '#title'         => t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#required'      => TRUE,
      '#min'           => 1,
    ];
    $elements['placeholder'] = [
      '#type'          => 'textfield',
      '#title'         => t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description'   => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('Textfield size: @size', ['@size' => $this->getSetting('size')]);

    if (!empty($this->getSetting('placeholder'))) {
      $summary[] = t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type'          => 'textfield',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#size'          => $this->getSetting('size'),
      '#placeholder'   => $this->getSetting('placeholder'),
      '#maxlength'     => $this->getFieldSetting('max_length'),
    ];

    $element['#element_validate'] = array(
      [$this, 'validateZipCode'],
    );

    return $element;
  }

  /**
   * Validates zip code element.
   *
   * @param array $element
   *   The zip code element.
   * @param FormStateInterface $formState
   *   The form state object.
   * @param array $form
   *   The build form.
   */
  public function validateZipCode(array $element, FormStateInterface $formState, array $form) {
    $zipCode = $element['value']['#value'];

    if (empty($zipCode) && $element['#required']) {
      $message = t('You must specify a value');
      $formState->setError($element, $message);
    }
    elseif (!empty($zipCode) && !static::isZipCodeValid($zipCode)) {
      $message = t('Given zip code is invalid');
      $formState->setError($element, $message);
    }
  }

  /**
   * Returns TRUE if zip code is valid, FALSE otherwise.
   *
   * @param mixed $zipCode
   *   The zip code value.
   *
   * @return bool
   *   TRUE if valid, FALSE otherwise.
   */
  public static function isZipCodeValid($zipCode) {
    $isValid = TRUE;
    $pattern = "#^[0-9]{5}$#";
    $specialCases = [
      '00000',
      '11111',
      '22222',
      '33333',
      '44444',
      '55555',
      '66666',
      '77777',
      '88888',
      '99999',
    ];

    if (!is_string($zipCode) || !preg_match($pattern, $zipCode) || in_array($zipCode, $specialCases)) {
      $isValid = FALSE;
    }

    return $isValid;
  }

}
