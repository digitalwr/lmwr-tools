<?php

namespace Drupal\lmwr_form\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Render\Element;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\lmwr_form\Service\LmwrFormPluginManager;
use Drupal\Core\Form\FormState;

/**
 * Provides a field type of baz.
 *
 * @FieldType(
 *   id = "lmwr_form_field",
 *   label = @Translation("Custom Form"),
 *   default_formatter = "lmwr_form_field_formatter",
 *   default_widget = "lmwr_form_field_widget"
 * )
 */
class LmwrFormField extends FieldItemBase implements FieldItemInterface {

  /**
   * LmwrFormField type name.
   */
  const LMWR_FORM_FIELD_TYPE = 'lmwr_form_field';

  /**
   * Field vars key in form state build info.
   */
  const KEY_FIELD_VARS = 'field_vars';

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'value' => array(
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string');

    return $properties;
  }

  /**
   * Check if the field is a LmwrFormField.
   *
   * @param array $vars
   *   Form variables.
   *
   * @return bool
   *   TRUE if field matches lmwr form field type, FALSE otherwise.
   */
  public static function isLmwrFormField(array $vars) {
    return $vars['element']['#field_type'] == self::LMWR_FORM_FIELD_TYPE;
  }

  /**
   * Init the field data with linked LmwrFormBase.
   *
   * @param array $vars
   *   Form variables.
   */
  public static function initFormField(array &$vars) {
    /** @var LmwrFormPluginManager $manager */
    $manager = \Drupal::service(LmwrFormPluginManager::SERVICE_NAME);

    // Prepare FormState.
    $form_state = new FormState();
    $form_state->addBuildInfo(self::KEY_FIELD_VARS, $vars);

    // Parse each item :
    foreach (Element::getVisibleChildren($vars['items']) as $itemKey) {
      if ($form_id = $vars['items'][$itemKey]['content']['#context']['value']) {
        /** @var FormBase $form */
        if ($form = $manager->getInstanceByFormId($form_id, $form_state)) {
          $vars['items'][$itemKey]['content']['#context']['value'] = $form;
        }
      }
    }
  }

}
