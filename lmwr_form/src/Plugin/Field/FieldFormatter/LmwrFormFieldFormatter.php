<?php

namespace Drupal\lmwr_form\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'field_example_simple_text' formatter.
 *
 * @FieldFormatter(
 *   id = "lmwr_form_field_formatter",
 *   module = "lmwr_form",
 *   label = @Translation("Simple text-based formatter"),
 *   field_types = {
 *     "lmwr_form_field"
 *   }
 * )
 */
class LmwrFormFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();

    foreach ($items as $delta => $item) {
      $elements[$delta] = array(
        '#type' => 'inline_template',
        '#template' => "{{value}}",
        '#context' => ["value" => $item->value],
      );
    }

    return $elements;
  }

}
