<?php

namespace Drupal\lmwr_form\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an LmwrFormAnnotation annotation object.
 *
 * @Annotation
 *
 * @ingroup lmwr_form
 */
class LmwrFormAnnotation extends Plugin {
  /**
   * The plugin ID == the form ID.
   *
   * @var string
   */
  public $id;

  /**
   * Readable name for field selector.
   *
   * @var string
   */
  public $name;

}
