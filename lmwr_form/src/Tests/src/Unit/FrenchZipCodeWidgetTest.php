<?php

namespace Drupal\lmwr_form\Tests;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\lmwr_form\Plugin\Field\FieldWidget\FrenchZipCodeWidget;
use Drupal\Tests\UnitTestCase;

/**
 * Class FrenchZipCodeWidgetTest.
 *
 * @group lmwr_form
 */
class FrenchZipCodeWidgetTest extends UnitTestCase {

  /**
   * The widget object to test.
   *
   * @var \Drupal\lmwr_form\Plugin\Field\FieldWidget\FrenchZipCodeWidget
   */
  private $widget;

  /**
   * Before each test.
   */
  public function setUp() {
    $plugin_id = '';
    $plugin_definition = '';
    $field_definition = $this->getMockBuilder(FieldDefinitionInterface::class)->disableOriginalConstructor()->getMock();
    $settings = [];
    $third_party_settings = [];
    $this->widget = new FrenchZipCodeWidget($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
  }

  /**
   * After each test.
   */
  public function tearDown() {
    $this->widget = NULL;
  }

  /**
   * Provide test data for zip code validation.
   *
   * @return array
   *   Test data.
   */
  public function zipCodeProvider() {
    return [
      ['00000', FALSE],
      ['11111', FALSE],
      ['22222', FALSE],
      ['33333', FALSE],
      ['44444', FALSE],
      ['55555', FALSE],
      ['66666', FALSE],
      ['77777', FALSE],
      ['88888', FALSE],
      ['99999', FALSE],
      ['56', FALSE],
      ['56sdfq', FALSE],
      [[], FALSE],
      ['44100', FALSE],
    ];
  }

  /**
   * Test method isZipCodeValid in several cases.
   *
   * @param mixed $zipCode
   *   Given zip code.
   * @param bool $expected
   *   Expected result.
   *
   * @dataProvider zipCodeProvider
   */
  public function testIsZipCodeValid($zipCode, $expected) {
    $actual = FrenchZipCodeWidget::isZipCodeValid($zipCode);

    $this->assertEquals($expected, $actual);
  }

}
