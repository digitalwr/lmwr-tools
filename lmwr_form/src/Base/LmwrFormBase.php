<?php

namespace Drupal\lmwr_form\Base;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\lmwr_form\Plugin\Field\FieldType\LmwrFormField;

/**
 * Class LmwrFormBase.
 *
 * @package lmwr_form
 */
abstract class LmwrFormBase extends FormBase implements LmwrFormBaseInterface {

  use LmwrFormTrait;

  /**
   * Return the form ID ( the form ID is the same as the plugin ID)
   *
   * @return mixed
   *   The form ID.
   */
  abstract public function getFormId();

  /**
   * Default build form : call buildForm{view_mode} if defined.
   *
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Suppression du cache sur le formulaire.
    $form['#cache'] = ['max-age' => 0];

    // Use building methods by view_mode.
    $method = 'buildForm' . ucfirst($this->getParentObjectViewMode($form_state));
    if (method_exists($this, $method)) {
      return $this->$method($form, $form_state);
    }
  }

  /**
   * Default build form : call submitForm{view_mode} if defined.
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Use building methods by view_mode :
    $method = 'submitForm' . ucfirst($this->getParentObjectViewMode($form_state));
    if (method_exists($this, $method)) {
      return $this->$method($form, $form_state);
    }
  }

  /**
   * Return plugin ID = the form ID.
   *
   * @return mixed
   *   The ID of the pugin.
   */
  public function getPluginId() {
    return $this->getFormId();
  }

  /**
   * Return plugin definition.
   *
   * @return array
   *   The plugin definition.
   */
  public function getPluginDefinition() {
    return [];
  }

}
