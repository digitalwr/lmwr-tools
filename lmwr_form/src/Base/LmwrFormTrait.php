<?php

namespace Drupal\lmwr_form\Base;

use Drupal\Core\Form\FormStateInterface;
use Drupal\lmwr_form\Plugin\Field\FieldType\LmwrFormField;
use Drupal\lmwr_tools\Service\LanguageService;

/**
 * Class LmwrFormTrait.
 *
 * @package Drupal\lmwr_tools
 */
trait LmwrFormTrait {

  /**
   * Return the field preprocess vars where the current form is defined.
   *
   * @param FormStateInterface $form_state
   *   The form object.
   *
   * @return mixed
   *   The vars of the LmwrFormField linking the current form.
   */
  public function getLmwrFormFieldVars(FormStateInterface $form_state) {
    return $form_state->getBuildInfo()[LmwrFormField::KEY_FIELD_VARS];
  }

  /**
   * Return the parent entity of the field where the current form is defined.
   *
   * @param FormStateInterface $form_state
   *   The form object.
   *
   * @return mixed
   *   The object containing the LmwrFormField linking the current form.
   */
  public function getFieldParentObject(FormStateInterface $form_state) {
    $object = $this->getLmwrFormFieldVars($form_state)['element']['#object'];
    if ($object) {
      return LanguageService::translate($object);
    }
    return NULL;
  }

  /**
   * Return the parent element view mode :.
   *
   * @param FormStateInterface $form_state
   *   The form object.
   *
   * @return mixed
   *   The Viewmode of the containing the LmwrFormField linking the current form.
   */
  public function getParentObjectViewMode(FormStateInterface $form_state) {
    return $this->getLmwrFormFieldVars($form_state)['element']['#view_mode'];
  }

}
