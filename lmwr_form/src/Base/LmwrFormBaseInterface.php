<?php

namespace Drupal\lmwr_form\Base;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface LmwrFormBaseInterface.
 *
 * @package lmwr_form
 */
interface LmwrFormBaseInterface extends PluginInspectionInterface {

}
