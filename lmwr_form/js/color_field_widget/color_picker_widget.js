Drupal.behaviors.lmwrFormColorPickerWidget = {
    attach: function (context, settings) {

        $ = jQuery;

        $(function () {

            $('input.color_picker').each(function () {
                $(this).removeClass('visually-hidden');
                var options = {
                    preferredFormat: "hex",
                    showInput: true
                };

                $(this).spectrum(options);
            });
        });
    }
};