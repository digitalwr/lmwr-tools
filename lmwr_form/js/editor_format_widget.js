Drupal.behaviors.lmwrFormEditorFormatWidget = {
    attach: function (context, settings) {

        $ = jQuery;

        $(function () {

            if (context !== document) {
                // AJAX request.
                return;
            }

            $('.lmwr_form_force_editor_type').each(function(){
                $( $(this).parents('.text-format-wrapper')[0]).addClass( 'lmwr_form_force_editor_type' );
            });
        });
    }
};