<?php

/**
 * @file
 * Contains lmwr_field_cluster_entity.page.inc.
 *
 * Page callback for Lmwr field cluster entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Lmwr field cluster entity templates.
 *
 * Default template: lmwr_field_cluster_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_lmwr_field_cluster_entity(array &$variables) {
  // Fetch LmwrFieldClusterEntity Entity Object.
  $lmwr_field_cluster_entity = $variables['elements']['#lmwr_field_cluster_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
