<?php

namespace Drupal\lmwr_field_cluster\Service;

use Drupal\Core\Language\LanguageInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class TranslationTools.
 *
 * @package Drupal\lmwr_field_cluster\Service
 */
class TranslationTools {

  const SERVICE_NAME = 'lmwr_cluster_field.translation_tools';

  const FORCE_PUBLICATION_KEY = 'publication';
  const FORCE_PUBLICATION_VALUE = 'force';

  protected $entityTypeToEmpty = ['lmwr_field_cluster_entity'];

  /**
   * Empty the untranslatable fields.
   *
   * Lorsqu'on va créer une traduction, afin de ne pas laisser croire à l'utilisateur
   * qu'il peut traduire une entité non traductible référencée (typiquement un lmwr_cluster_field)
   * au moment d'arriver sur le formulaire d'édition de la traduction :
   *  - 1. on supprime les liaisons vers les éléments non traductibles
   *  - 2. on sauvegarde la traduction en mode non publié
   *  - 3. on redirige vers la page de modification de la traduction.
   *  - 4. on affiche les messages liés au vidage des éléments non traductibles.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\node\Entity\Node $node
   *   The node.
   */
  public function initNodeFormTranslationCreation(array &$form, Node $node) {

    // In the add case, we don't need to empty the reference.
    if (is_null($node->id())) {
      return;
    }

    $translationStatus = $node->getTranslationStatus($node->language()
      ->getId());

    // If translation status is locked (in progress).
    if ($translationStatus == LanguageInterface::STATE_LOCKED) {

      // Get untranslatable fields.
      $untranslatableFields = $this->getUntranslatableFields($form);
      if (!empty($untranslatableFields)) {

        // For each untranslatable field, we empty the references.
        foreach ($untranslatableFields as $untranslatableField) {
          $this->emptyUntranslatableFields($node, $untranslatableField);
        }

        // We unpublish the current new translation in order to not publish unwanted content.
        $node->status = NodeInterface::NOT_PUBLISHED;
        $node->save();

        // We redirect to the edition page.
        $request = RedirectResponse::create('/' . $node->language()
          ->getId() . '/node/' . $node->id() . '/edit?' . self::FORCE_PUBLICATION_KEY . '=' . self::FORCE_PUBLICATION_VALUE);
        $request->send();
        exit;
      }
    }
    elseif ($translationStatus == LanguageInterface::STATE_CONFIGURABLE &&
      \Drupal::request()
        ->get(self::FORCE_PUBLICATION_KEY) == self::FORCE_PUBLICATION_VALUE
    ) {
      // Show alert messages.
      \drupal_set_message(t('Certains éléments non-traductibles n\'ont pas été reportés sur cette traduction. Ils doivent être recréés dans la langue de la traduction.'));
      \drupal_set_message(t('Attention! Cette traduction n\'est pas publiée par défaut.'), 'warning');
    }
  }

  /**
   * Return true if the field must be emptied on translation.
   *
   * @param array $formField
   *   The field array.
   *
   * @return bool
   *   The translatable state.
   */
  protected function fieldIsUntranslatable(array $formField) {
    if (array_key_exists('widget', $formField)) {
      if (array_key_exists('entities', $formField['widget'])) {
        if (array_key_exists('#entity_type', $formField['widget']['entities'])) {
          return in_array($formField['widget']['entities']['#entity_type'], $this->entityTypeToEmpty);
        }
      }
    }
    return FALSE;
  }

  /**
   * Empty the field for the passed node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node.
   * @param string $untranslatableField
   *   The fieldName to empty.
   */
  protected function emptyUntranslatableFields(Node $node, $untranslatableField) {
    for ($i = $node->get($untranslatableField)
      ->count() - 1; $i > -1; $i--) {
      $node->get($untranslatableField)->removeItem($i);
    }
  }

  /**
   * Return the list of untranslatableFields.
   *
   * @param array $form
   *   The form.
   *
   * @return array
   *   The fields list.
   */
  protected function getUntranslatableFields(array $form) {
    $untranslatableFields = [];
    foreach ($form as $fieldName => $formField) {
      if (is_array($formField) && $this->fieldIsUntranslatable($formField)) {
        $untranslatableFields[] = $fieldName;
      }
    }

    return $untranslatableFields;
  }

}
