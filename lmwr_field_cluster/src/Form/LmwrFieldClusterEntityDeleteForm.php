<?php

namespace Drupal\lmwr_field_cluster\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Lmwr field cluster entity entities.
 *
 * @ingroup lmwr_field_cluster
 */
class LmwrFieldClusterEntityDeleteForm extends ContentEntityDeleteForm {


}
