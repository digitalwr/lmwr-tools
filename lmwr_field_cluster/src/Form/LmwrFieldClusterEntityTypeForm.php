<?php

namespace Drupal\lmwr_field_cluster\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LmwrFieldClusterEntityTypeForm.
 *
 * @package Drupal\lmwr_field_cluster\Form
 */
class LmwrFieldClusterEntityTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $lmwr_field_cluster_entity_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $lmwr_field_cluster_entity_type->label(),
      '#description' => $this->t("Label for the Lmwr field cluster entity type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $lmwr_field_cluster_entity_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\lmwr_field_cluster\Entity\LmwrFieldClusterEntityType::load',
      ],
      '#disabled' => !$lmwr_field_cluster_entity_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $lmwr_field_cluster_entity_type = $this->entity;
    $status = $lmwr_field_cluster_entity_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Lmwr field cluster entity type.', [
          '%label' => $lmwr_field_cluster_entity_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Lmwr field cluster entity type.', [
          '%label' => $lmwr_field_cluster_entity_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($lmwr_field_cluster_entity_type->toUrl('collection'));
  }

}
