<?php

namespace Drupal\lmwr_field_cluster\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Lmwr field cluster entity edit forms.
 *
 * @ingroup lmwr_field_cluster
 */
class LmwrFieldClusterEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\lmwr_field_cluster\Entity\LmwrFieldClusterEntity */
    $form = parent::buildForm($form, $form_state);

    // Delete author.
    $form['user_id']['#access'] = FALSE;

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Lmwr field cluster entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Lmwr field cluster entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.lmwr_field_cluster_entity.canonical', ['lmwr_field_cluster_entity' => $entity->id()]);
  }

}
