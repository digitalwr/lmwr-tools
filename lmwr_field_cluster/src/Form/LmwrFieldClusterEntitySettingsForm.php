<?php

namespace Drupal\lmwr_field_cluster\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LmwrFieldClusterEntitySettingsForm.
 *
 * @package Drupal\lmwr_field_cluster\Form
 *
 * @ingroup lmwr_field_cluster
 */
class LmwrFieldClusterEntitySettingsForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'LmwrFieldClusterEntity_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

  /**
   * Defines the settings form for Lmwr field cluster entity entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['LmwrFieldClusterEntity_settings']['#markup'] = 'Settings form for Lmwr field cluster entity entities. Manage field settings here.';
    return $form;
  }

}
