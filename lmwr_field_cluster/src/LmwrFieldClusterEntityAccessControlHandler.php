<?php

namespace Drupal\lmwr_field_cluster;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Lmwr field cluster entity entity.
 *
 * @see \Drupal\lmwr_field_cluster\Entity\LmwrFieldClusterEntity.
 */
class LmwrFieldClusterEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\lmwr_field_cluster\Entity\LmwrFieldClusterEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished lmwr field cluster entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published lmwr field cluster entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit lmwr field cluster entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete lmwr field cluster entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add lmwr field cluster entity entities');
  }

}
