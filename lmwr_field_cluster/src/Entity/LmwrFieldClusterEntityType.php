<?php

namespace Drupal\lmwr_field_cluster\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Lmwr field cluster entity type entity.
 *
 * @ConfigEntityType(
 *   id = "lmwr_field_cluster_entity_type",
 *   label = @Translation("Lmwr field cluster entity type"),
 *   handlers = {
 *     "list_builder" = "Drupal\lmwr_field_cluster\LmwrFieldClusterEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\lmwr_field_cluster\Form\LmwrFieldClusterEntityTypeForm",
 *       "edit" = "Drupal\lmwr_field_cluster\Form\LmwrFieldClusterEntityTypeForm",
 *       "delete" = "Drupal\lmwr_field_cluster\Form\LmwrFieldClusterEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\lmwr_field_cluster\LmwrFieldClusterEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "lmwr_field_cluster_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "lmwr_field_cluster_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/lmwr_field_cluster_entity_type/{lmwr_field_cluster_entity_type}",
 *     "add-form" = "/admin/structure/lmwr_field_cluster_entity_type/add",
 *     "edit-form" = "/admin/structure/lmwr_field_cluster_entity_type/{lmwr_field_cluster_entity_type}/edit",
 *     "delete-form" = "/admin/structure/lmwr_field_cluster_entity_type/{lmwr_field_cluster_entity_type}/delete",
 *     "collection" = "/admin/structure/lmwr_field_cluster_entity_type"
 *   }
 * )
 */
class LmwrFieldClusterEntityType extends ConfigEntityBundleBase implements LmwrFieldClusterEntityTypeInterface {

  /**
   * The Lmwr field cluster entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Lmwr field cluster entity type label.
   *
   * @var string
   */
  protected $label;

}
