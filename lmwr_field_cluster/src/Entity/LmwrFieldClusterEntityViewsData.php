<?php

namespace Drupal\lmwr_field_cluster\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Lmwr field cluster entity entities.
 */
class LmwrFieldClusterEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
