<?php

namespace Drupal\lmwr_field_cluster\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Lmwr field cluster entity entities.
 *
 * @ingroup lmwr_field_cluster
 */
interface LmwrFieldClusterEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Lmwr field cluster entity type.
   *
   * @return string
   *   The Lmwr field cluster entity type.
   */
  public function getType();

  /**
   * Gets the Lmwr field cluster entity name.
   *
   * @return string
   *   Name of the Lmwr field cluster entity.
   */
  public function getName();

  /**
   * Sets the Lmwr field cluster entity name.
   *
   * @param string $name
   *   The Lmwr field cluster entity name.
   *
   * @return \Drupal\lmwr_field_cluster\Entity\LmwrFieldClusterEntityInterface
   *   The called Lmwr field cluster entity entity.
   */
  public function setName($name);

  /**
   * Gets the Lmwr field cluster entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Lmwr field cluster entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Lmwr field cluster entity creation timestamp.
   *
   * @param int $timestamp
   *   The Lmwr field cluster entity creation timestamp.
   *
   * @return \Drupal\lmwr_field_cluster\Entity\LmwrFieldClusterEntityInterface
   *   The called Lmwr field cluster entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Lmwr field cluster entity published status indicator.
   *
   * Unpublished Lmwr field cluster entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Lmwr field cluster entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Lmwr field cluster entity.
   *
   * @param bool $published
   *   TRUE to set this Lmwr field cluster entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\lmwr_field_cluster\Entity\LmwrFieldClusterEntityInterface
   *   The called Lmwr field cluster entity entity.
   */
  public function setPublished($published);

}
