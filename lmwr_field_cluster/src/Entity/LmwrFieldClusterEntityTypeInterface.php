<?php

namespace Drupal\lmwr_field_cluster\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Lmwr field cluster entity type entities.
 */
interface LmwrFieldClusterEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
