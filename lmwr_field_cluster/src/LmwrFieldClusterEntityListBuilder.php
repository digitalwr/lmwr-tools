<?php

namespace Drupal\lmwr_field_cluster;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Lmwr field cluster entity entities.
 *
 * @ingroup lmwr_field_cluster
 */
class LmwrFieldClusterEntityListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Lmwr field cluster entity ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\lmwr_field_cluster\Entity\LmwrFieldClusterEntity */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.lmwr_field_cluster_entity.edit_form', array(
          'lmwr_field_cluster_entity' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
