Drupal.behaviors.lmwrFacebook = {
    attach: function (context, settings) {
        if (context !== document) {
            // AJAX request.
            return;
        }

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=" + settings.fbAppVersion;
            if (settings.fbAppId) {
                js.src += "&appId=" + settings.fbAppId;
            }
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk', settings));
    }
};
