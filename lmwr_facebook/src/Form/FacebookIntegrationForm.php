<?php

namespace Drupal\lmwr_facebook\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContactRequestForm.
 *
 * @package Drupal\batir_admin\Form
 */
class FacebookIntegrationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'facebookIntegrationForm';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App ID'),
      '#required' => TRUE,
      '#default_value' => \Drupal::config('lmwr.facebook')->get('appID'),
    ];

    $facebookAppVersions = [
      'v2.2' => 'v2.2',
      'v2.3' => 'v2.3',
      'v2.4' => 'v2.4',
      'v2.5' => 'v2.5',
      'v2.6' => 'v2.6',
      'v2.7' => 'v2.7',
      'v2.8' => 'v2.8',
    ];

    $form['app_version'] = [
      '#type' => 'select',
      '#title' => $this->t('API version'),
      '#options' => $facebookAppVersions,
      '#required' => TRUE,
      '#default_value' => \Drupal::config('lmwr.facebook')->get('appVersion'),
    ];

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::configFactory()->getEditable('lmwr.facebook')->set('appID', $form_state->getValue('app_id'))->save();
    \Drupal::configFactory()->getEditable('lmwr.facebook')->set('appVersion', $form_state->getValue('app_version'))->save();

    drupal_set_message($this->t('The changes have been saved.'));
  }

}
