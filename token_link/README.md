#Token link 

Ce module permet de gérer les liens du ckeditor dynamiquement.

En prenant en compte une liaison entre l'élément lié et l'élément liant. Lorsque l'élément lié change d'url
l'élément liant rafraichit son cache, et lors du prochain affichage, la nouvelle url est prise en compte, sans avoir
à modifier le contenu du CKEditor.


##Dépendances
le module dépend de 
- ckeditor
- drupal/ckeditor_entity_link
- shortcut
- path
- pathauto


## Configuration
Le mieux c'est de virer la gestion des liens de bases et ne laisser que token_link pour que 
l'utilisateur ne confonde pas les autres.

1. Ajouter le bouton token_link à la barre d'édition
2. Activer les shortcodes
3. Activer le shortcode token_link

## Plus plus
- possibilité d'ajouter des lines externes
- possibilité d'ajouter des ancres
