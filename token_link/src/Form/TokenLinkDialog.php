<?php

namespace Drupal\token_link\Form;

use Drupal\ckeditor_entity_link\Form\CKEditorEntityLinkDialog;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\filter\Entity\FilterFormat;
use Drupal\token_link\Service\TokenLinkManager;

/**
 * Class TokenLinkDialog.
 *
 * @package Drupal\token_link\Form
 */
class TokenLinkDialog extends CKEditorEntityLinkDialog {

  const FIELD_EXTERNAL = 'external_link';

  /**
   * {@inheritdoc}
   */
  public function getUrl(EntityInterface $entity) {
    return "[tl id='" . $entity->getEntityTypeId() . ":" . $entity->id() . "'][/tl]";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, FilterFormat $filter_format = NULL) {
    // Récupération des valeurs par défault.
    $defaultValues = $this->getDefaultValues($form_state);

    $form = parent::buildForm($form, $form_state, $filter_format);

    // Ajout des liens externes.
    $form['entity_type']['#options'][static::FIELD_EXTERNAL] = t('Lien externe');
    $form['entity_type']['#default_value'] = $defaultValues['entity_type'];

    if ($form_state->getValue('entity_type') == static::FIELD_EXTERNAL
      || $defaultValues['entity_type'] == static::FIELD_EXTERNAL) {
      $form['entity_id'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Lien externe'),
        '#default_value' => $defaultValues['entity_id'] ?: '//',
        '#prefix'        => '<div id="entity-id-wrapper">',
        '#suffix'        => '</div>',
      );
    }
    else {
      if ($defaultValues['entity_id']) {
        $object = \Drupal::service(TokenLinkManager::SERVICE_NAME)
          ->getEntityFromId($defaultValues['entity_type'] . ':' . $defaultValues['entity_id']);
        $form['entity_id']['#default_value'] = $object;
        $form['entity_id']['#target_type'] = $object->getEntityTypeId();
      }
    }

    // Ajout de l'ancre.
    $form['anchor_wrapper'] = [
      '#type'  => 'details',
      '#title' => t('Anchor'),
      '#open'  => FALSE,
    ];
    $form['anchor_wrapper']['anchor'] = [
      '#type'          => 'textfield',
      '#title'         => t('Anchor'),
      '#default_value' => $defaultValues['anchor'],
      '#description'   => 'ex: "#footer"',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type'   => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand('#ckeditor-entity-link-dialog-form', $form));
    }
    else {
      if ($form_state->getValue('entity_type') == static::FIELD_EXTERNAL) {
        $values = array(
          'attributes' => array(
            'href' => $form_state->getValue('entity_id'),
          ) + $form_state->getValue('attributes', []),
          'anchor'     => $form_state->getValue('anchor_wrapper')['anchor'],
        );
      }
      else {
        $entity = \Drupal::entityTypeManager()
          ->getStorage($form_state->getValue('entity_type'))
          ->load($form_state->getValue('entity_id'));

        $values = array(
          'attributes' => array(
            'href' => $this->getUrl($entity),
          ) + $form_state->getValue('attributes', []),
          'anchor'     => $form_state->getValue('anchor_wrapper')['anchor'],
        );
      }

      $response->addCommand(new EditorDialogSave($values));
      $response->addCommand(new CloseModalDialogCommand());
    }

    // Hook pour altérer les données du lien.
    $info = $form_state->getValues();
    \Drupal::moduleHandler()
      ->alter('token_link_inserted_data', $values, $info);

    return $response;
  }

  /**
   * REtourne le tableau de valeurs par défault.
   *
   * @param FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The values.
   */
  protected function getDefaultValues(FormStateInterface $form_state) {
    $defaultValues = $form_state->getUserInput()['editor_object'];

    // Récupération du href.
    $hrefData = explode('#', $defaultValues['href']);
    $href = reset($hrefData);

    // Gestion de l'ancre.
    if (count($hrefData) > 1) {
      $anchor = end($hrefData);
    }
    else {
      $anchor = NULL;
    }

    // Gestion du type de contenu.
    if (preg_match_all('~(["\'])([^"\']+)\1~', $href, $arr)) {
      $data = explode(':', end($arr)[0]);
      list($entity_type, $entity_id) = $data;
    }
    elseif (!empty($href) || isset($anchor)) {
      $entity_type = static::FIELD_EXTERNAL;
      $entity_id = $href;
    }
    else {
      $entity_type = 'node';
      $entity_id = NULL;
    }

    return [
      'anchor'      => $anchor ?: '',
      'entity_type' => $entity_type,
      'entity_id'   => $entity_id,
    ];
  }

}
