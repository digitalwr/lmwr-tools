<?php

namespace Drupal\token_link\Service;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Entity\EntityInterface;
use Drupal\lmwr_tools\Service\LanguageService;

/**
 * Class TokenLinkManager.
 *
 * @package Drupal\token_link\Service
 */
class TokenLinkManager {

  const SERVICE_NAME = 'token_link.manager';

  const TABLE = 'token_link_usage';
  const FIELD_ENTITY_ID = 'entity_id';
  const FIELD_ENTITY_TYPE = 'entity_type';
  const FIELD_REFERER_ID = 'referer_id';
  const FIELD_REFERER_TYPE = 'referer_type';

  /**
   * Ajoute un référetant à l'entité linkée.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Entity\EntityInterface $referer
   *   The referer.
   */
  public function addReferer(EntityInterface $entity = NULL, EntityInterface $referer = NULL) {

    if ($entity && $referer) {
      $data = [
        static::FIELD_ENTITY_ID    => $entity->id(),
        static::FIELD_ENTITY_TYPE  => $entity->getEntityTypeId(),
        static::FIELD_REFERER_ID   => $referer->id(),
        static::FIELD_REFERER_TYPE => $referer->getEntityTypeId(),
      ];

      try {
        $query = \Drupal::database()->insert(static::TABLE)
          ->fields(array_keys($data))
          ->values($data)
          ->execute();
      }
      catch (\Exception $e) {
        // Mute exception...
        // En fait ici, on arrive si l'élément est déjà en db.
        // je pense qu'il est moins couteux de gérer l'exception
        // Plutot que de faire une requête de test puis une requete d'insertion.
      }
    }
  }

  /**
   * Retourne l'entité depuis l'id passé en attribut.
   *
   * @param string $id
   *   L'id (ex:  'node:3').
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   L'entité.
   */
  public function getEntityFromId($id) {
    list($type, $id) = explode(':', $id);

    try {
      return LanguageService::load($type, $id);
    }
    catch (\Exception $e) {
      // Mute exception...
    }
    return NULL;
  }

  /**
   * Action à la modification de path d'un élément.
   *
   * @param array $path
   *   The path.
   */
  public function onPathUpdate(array $path = []) {
    try {
      list($entityType, $entityId) = explode(':', $this->getIdByPath($path));
      $refererIds = $this->getRefererIds($entityType, $entityId);
      Cache::invalidateTags($refererIds);
    }
    catch (\Exception $e) {
      // Mute exception...
    }

  }

  /**
   * Retourne l'id (node:3) depuis le path.
   *
   * @param array $path
   *   The path.
   *
   * @return string
   *   The id.
   */
  public function getIdByPath(array $path = []) {
    list($null, $type, $id) = explode('/', $path['source']);
    return $type . ':' . $id;
  }

  /**
   * Retourne la liste des ids des referer.
   *
   * @param string $entityType
   *   The type.
   * @param string $entityId
   *   The id.
   *
   * @return array
   *   The ids list.
   */
  public function getRefererIds($entityType, $entityId) {
    $query = \Drupal::database()->select(static::TABLE, 't');
    $query->addExpression('CONCAT(' . static::FIELD_REFERER_TYPE . ', \':\', ' . static::FIELD_REFERER_ID . ')', 'result');
    $query->condition(static::FIELD_ENTITY_TYPE, $entityType);
    $query->condition(static::FIELD_ENTITY_ID, $entityId);

    try {
      return $query->execute()->fetchCol();
    }
    catch (\Exception $e) {
      return [];
    }
  }

}
