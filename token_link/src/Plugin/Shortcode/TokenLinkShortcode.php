<?php

namespace Drupal\token_link\Plugin\Shortcode;

use Cocur\Slugify\Slugify;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\Language;
use Drupal\lmwr_tools\Service\Misc;
use Drupal\shortcode\Plugin\ShortcodeBase;
use Drupal\token_link\Service\TokenLinkManager;

/**
 * Provides a shortcode for bootstrap columns.
 *
 * @Shortcode(
 *   id = "tl",
 *   title = @Translation("Token link shortcode"),
 *   description = @Translation("Display a url from token.")
 * )
 */
class TokenLinkShortcode extends ShortcodeBase {

  /**
   * Slugifier.
   *
   * @var Slugify
   */
  protected $slugifier;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function process(array $attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    if (array_key_exists('id', $attributes) && !empty($attributes['id'])) {
      if ($url = $this->getUrlFromId($attributes['id'])) {
        return $url;
      }
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return ' ';
  }

  /**
   * Retourne  l'url depuis l'id passé en paramètre.
   *
   * @param string $id
   *   L'id (ex:  'node:3').
   *
   * @return \Drupal\Core\GeneratedUrl|null|string
   *   L'url.
   */
  protected function getUrlFromId($id) {
    /** @var ContentEntityBase $entity */
    if ($entity = $this->getEntityFromId($id)) {
      $this->addReferer($entity);
      return $entity->url();
    }
    return NULL;
  }

  /**
   * Retourne l'entité depuis l'id passé en attribut.
   *
   * @param string $id
   *   L'id (ex:  'node:3').
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   L'entité.
   */
  protected function getEntityFromId($id) {
    return \Drupal::service(TokenLinkManager::SERVICE_NAME)
      ->getEntityFromId($id);
  }

  /**
   * Ajoute un referer.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  protected function addReferer(EntityInterface $entity) {
    \Drupal::service(TokenLinkManager::SERVICE_NAME)
      ->addReferer($entity, Misc::getCurrentPageNode());
  }

}
