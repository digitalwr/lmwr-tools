<?php

namespace Drupal\token_link\Plugin\CKEditorPlugin;

use Drupal\ckeditor_entity_link\Plugin\CKEditorPlugin\EntityLink;

/**
 * Defines the "tokenlink" plugin.
 *
 * @CKEditorPlugin(
 *   id = "tokenlink",
 *   label = @Translation("Token link"),
 * )
 */
class TokenLink extends EntityLink {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'token_link') . '/js/plugins/tokenlink/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $path = drupal_get_path('module', 'token_link') . '/js/plugins/tokenlink';
    return array(
      'TokenLink' => array(
        'label' => t('Token Link'),
        'image' => $path . '/tokenlink.png',
      ),
    );
  }

}
